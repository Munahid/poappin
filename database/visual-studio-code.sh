#!/bin/bash

POAPPIN_NAME="visual-studio-code"
POAPPIN_DESCRIPTION="Lightweight but powerful source code editor"
POAPPIN_HOMEPAGE="https://code.visualstudio.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # Source code, MIT license: https://github.com/microsoft/vscode
  # https://code.visualstudio.com/#alt-downloads
  # https://code.visualstudio.com/download
  # Clicking a link opens the same page and refreshes via javascript or
  # iframe (?) to: https://go.microsoft.com/fwlink/?Linkid=850641 *
  # I don't know if this link is changing between the different releases.
  # Another link I found was: https://github.com/microsoft/vscode/issues/74732
  # which links to:
  # https://update.code.visualstudio.com/latest/win32-x64-user/stable
  # https://update.code.visualstudio.com/latest/win32-x64/stable
  # https://update.code.visualstudio.com/latest/win32-x64-archive/stable *
  # https://update.code.visualstudio.com/latest/linux-deb-x64/stable
  # https://update.code.visualstudio.com/latest/linux-rpm-x64/stable
  # https://update.code.visualstudio.com/latest/linux-x64/stable
  # https://update.code.visualstudio.com/latest/darwin/stable
  # Both 64 bit zip archive links are redirecting to the same target URL.

  POAPPIN_URL="https://update.code.visualstudio.com/latest/win32-x64-archive/stable"

  # we use wget -qS to get the http headers (like curl -IL) to find out the exact filename.
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://az764295.vo.msecnd.net/stable/d5e9aa0227e057a60c82568bf31c04730dc15dcd/VSCode-win32-x64-1.47.0.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(VSCode-win.*\.zip\)$/\1/p'`
  # VSCode-win32-x64-1.47.0.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/VSCode-win32-x64-\([0-9].*[0-9]\)\.zip$/\1/p'`
  # 1.47.0
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  # Change the setup into portable mode.
  # https://code.visualstudio.com/docs/editor/portable

  mkdir -p data/{user-data,extensions}     # Contains %APPDATA%\Code and %USERPROFILE%\.vscode\extensions
  #mkdir tmp # else system temp is used.
}
