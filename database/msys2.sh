#!/bin/bash

POAPPIN_NAME="msys2"
POAPPIN_DESCRIPTION="Software Distribution and Building Platform for Windows"
POAPPIN_HOMEPAGE="https://www.msys2.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Compared to the official installation program (msys2-x86_64-20200720.exe),
# the archive is not complete. Various files are not included:
# components.xml, InstallationLog.txt, installer.dat, installerResources, maintenancetool.*, network.xml.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/msys2-base-x86_64-\([0-9].*\)\.tar\.xz/\1/p'`
  # 20200720
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://repo.msys2.org/distrib/x86_64/"`

  POAPPIN_URL="https://repo.msys2.org/distrib/x86_64/"`echo "${DATA}" | sed -n 's/.*<a href="\(msys2-base-x86_64-20......\.tar\.xz\)">msys2.*/\1/p' | tail -n 1`
  # https://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-20200720.tar.xz

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/x86_64\/\(msys2-.*\.xz\)/\1/p'`
  # msys2-base-x86_64-20200720.tar.xz

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_FILENAME/.xz/}"
  rm "${POAPPIN_FILENAME/.xz/}"
  mv msys64/* .
  rm -rf msys64
}
