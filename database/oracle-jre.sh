#!/bin/bash

POAPPIN_NAME="oracle-jre"
POAPPIN_DESCRIPTION="Oracle's Java Runtime Environment (JRE)"
POAPPIN_HOMEPAGE="https://www.oracle.com/java/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Non-functional ATM, needs an oracle.com account to download.

function poappin_app_check {
  POAPPIN_URL=`${POAPPIN_GET_PAGE} "https://www.oracle.com/java/technologies/javase-jre8-downloads.html" | sed -n "s/.*data-file='\(\/\/download.*.tar.gz\)' data-license=.*/https:\1/p" | sed 's/otn/otn-pub/g' | grep "windows-x64.tar.gz"`
  # https://download.oracle.com/otn-pub/java/jdk/8u251-b08/3d5a2bb8f8d4428bbe94aed7ec7ae784/jre-8u251-windows-x64.tar.gz

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(jre-8u.*\.gz\)$/\1/p'`
  # jre-8u251-windows-x64.tar.gz

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/jre-\([0-9].*[0-9]\)-windows-x64.*$/\1/p'`
  # 8u251
}

# If downloading fails without a reason, read here:
# https://gist.github.com/wavezhang/ba8425f24a968ec9b2a8619d7c2d86a6

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_FILENAME/.tar.gz/.tar}"
  rm ${POAPPIN_FILENAME/.tar.gz/.tar}
  sync
  mv jre*/* .
  sync
  rm -rf jre*
}
