#!/bin/bash

POAPPIN_NAME="coretemp"
POAPPIN_DESCRIPTION="Program to monitor processor temperature and other vital information"
POAPPIN_HOMEPAGE="https://www.alcpu.com/CoreTemp/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/CoreTemp64-\([0-9].*\)\.zip/\1/p'`
  # 1.15.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.alcpu.com/CoreTemp/"`

  # Download the standalone version of Core Temp:&nbsp;<a href="CoreTemp32.zip">32 Bit</a>&nbsp;<a href="CoreTemp64.zip">64 Bit</a><br>
  # <a href="languages/de-DE.lng">German</a>&nbsp;|
  POAPPIN_URL="https://www.alcpu.com/CoreTemp/"`echo "${DATA}" | sed -n 's/^.*standalone version.*a href="\(CoreTemp64.zip\)">64 Bit.*$/\1/p'`
  # https://www.alcpu.com/CoreTemp/CoreTemp64.zip

  # <b>Core Temp 1.15.1:<br></b>
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<b>Core Temp \([0-9].*\):<br><\/b>.*$/\1/p'`
  # 1.15.1

  POAPPIN_FILENAME="CoreTemp64-${POAPPIN_VERSION}.zip"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/CoreTemp-${POAPPIN_VERSION}-de-DE.lng" "https://www.alcpu.com/CoreTemp/languages/de-DE.lng"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mkdir languages
  cp "${POAPPIN_DOWNLOADS_DIR}/CoreTemp-${POAPPIN_VERSION}-de-DE.lng" \
     "${POAPPIN_APPS_DIR}/${POAPPIN_NAME}-${POAPPIN_VERSION}/languages/de-DE.lng"

  #sed -i 's/AutoUpdateCheck=1;/AutoUpdateCheck=0;/g' CoreTemp.ini
  #sed -i 's/Language=;/Language=Deutsch;/g' CoreTemp.ini
  cat <<EOT >> CoreTemp.ini
[General]
Language=Deutsch;
AutoUpdateCheck=0;
EOT

}
