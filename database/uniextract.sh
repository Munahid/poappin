#!/bin/bash

POAPPIN_NAME="uniextract"
POAPPIN_DESCRIPTION="Decompresses and extracts files from any type of archive or installer"
POAPPIN_HOMEPAGE="https://www.legroom.net/software/uniextract"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Needs latest 7-zip e.g. 19.xx, not 16.xx (MSYS2).

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.legroom.net/software/uniextract"`

  # <p><a href="/scripts/download.php?file=uniextract161_noinst">UniExtract Binary Archive</a>
  POAPPIN_URL="https://www.legroom.net"`echo "${DATA}" | sed -n 's/.*<a href="\(\/.*download.*file=uniextract.*_noinst\)">.*Binary Archive.*/\1/p'`
  # https://www.legroom.net/scripts/download.php?file=uniextract161_noinst

  # We look into the http headers to find out the exact filename. Location is http, not https!
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(http.*\)$/\1/p'`
  # http://www.legroom.net/files/software/uniextract161_noinst.rar

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*files\/software\/\(uniextract.*noinst\.rar\)/\1/p'`
  # uniextract161_noinst.rar

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/uniextract\([0-9].*\)_noinst\.rar/\1/p' | sed -r 's/.{1}/&./g; s/\.$//'`
  # 1.6.1
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME/\.rar/-German.ini}" \
    "https://www.legroom.net/files/software/ulang/German.ini"
}

function poappin_app_install {
  # TODO: integrate with the Windows Explorer context menu (see setup)

  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME/\.rar/-German.ini}" German.ini
  sed -i 's/language=English/language=German/' UniExtract.ini
}
