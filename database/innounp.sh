#!/bin/bash

POAPPIN_NAME="innounp"
POAPPIN_DESCRIPTION="Inno Setup Unpacker"
POAPPIN_HOMEPAGE="http://innounp.sourceforge.net/"
POAPPIN_LICENSE="GNU GPL + other"
POAPPIN_LICENSE_URL="http://innounp.sourceforge.net/#Copyrights"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# See too: https://sourceforge.net/projects/innounp/files/other%20stuff/Setup%20Factory%20unpacker/

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/innounp\([0-9].*\)\.rar/\1/p' | sed -n 's/\([0-9]\)\(.*\)/\1.\2/p'`
  # 0.49
}

function poappin_app_check {
  # <a class="button green big-text download with-sub-label extra-wide" href="/projects/innounp/files/latest/download" title="/innounp/innounp 0.49/innounp049.rar:  released on 2019-05-16 18:03:56 UTC">

  DATA=`${POAPPIN_GET_PAGE} "https://sourceforge.net/projects/innounp/files/innounp/"`

  POAPPIN_URL="https://sourceforge.net/projects/innounp/files/latest/download"

  POAPPIN_FILENAME=`echo "${DATA}" | sed -n 's/.*a class=.* href="\/projects\/innounp\/files\/.*" title="\/innounp\/.*\/\(innounp.*\.rar\)\:  released on .*$/\1/p'`
  # innounp049.rar

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
