#!/bin/bash

POAPPIN_NAME="innoextract"
POAPPIN_DESCRIPTION="Extracts installers created by Inno Setup 1.2.10 to 6.0.5"
POAPPIN_HOMEPAGE="https://constexpr.org/innoextract/"
POAPPIN_LICENSE="zlib/libpng"
POAPPIN_LICENSE_URL="https://raw.githubusercontent.com/dscharrer/innoextract/master/LICENSE"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/innoextract-\([0-9].*\)-windows\.zip/\1/p'`
  # 1.8
}

function poappin_app_check {
  # <a href="files/innoextract-1.8-windows.zip">innoextract <span itemprop="operatingSystem">Windows</span> Binaries</a>
  # <a href="https://github.com/dscharrer/innoextract/releases/download/1.8/innoextract-1.8-windows.zip">(mirror)</a>

  DATA=`${POAPPIN_GET_PAGE} "https://constexpr.org/innoextract/"`

  POAPPIN_URL="https://constexpr.org/innoextract/"`echo "${DATA}" | sed -n 's/.*a href="\(files\/innoextract-.*\.zip\)">innoextract.*Windows.*Binaries.*$/\1/p'`
  # https://constexpr.org/innoextract/files/innoextract-1.8-windows.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*files\/\(innoextract-.*\.zip\)/\1/p'`
  # innoextract-1.8-windows.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
