#!/bin/bash

POAPPIN_NAME="oracle-jdk"
POAPPIN_DESCRIPTION="Oracle's Java Development Kit (JDK)"
POAPPIN_HOMEPAGE="https://www.oracle.com/java/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Non-functional ATM, needs an oracle.com account to download.

function poappin_app_check {
  POAPPIN_URL="https://www.oracle.com/java/technologies/javase-jdk14-downloads.html"
  # https://www.oracle.com/java/technologies/javase-downloads.html
  # https://www.oracle.com/java/technologies/javase-jdk14-doc-downloads.html

  # <a class="license-link icn-download-locked" data-file="//download.oracle.com/otn-pub/java/jdk/12.0.2+10/e482c34c86bd4bf8b56c0b35558996b9/jdk-12.0.2_windows-x64_bin.zip" data-license="141" href="#license-lightbox" rel="lightbox" data-theme="light" data-ltbxclass="license-lightbox">jdk-12.0.2_windows-x64_bin.zip</a>

  POAPPIN_URL=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}" | sed -n "s/.*data-file='\(\/\/download.*.zip\)' data-license=.*/https:\1/p" | grep "windows-x64_bin.zip"`
  # https://download.oracle.com/otn-pub/java/jdk/14.0.1+7/664493ef4a6946b186ff29eb326336a2/jdk-14.0.1_windows-x64_bin.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(jdk-.*\.zip\)$/\1/p'`
  # jdk-14.0.1_windows-x64_bin.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/jdk-\([0-9].*[0-9]\)_windows-x64.*$/\1/p'`
  # 14.0.1
}

# If downloading fails without a reason, read here:
# https://gist.github.com/wavezhang/ba8425f24a968ec9b2a8619d7c2d86a6

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv jdk*/* .
  rm -rf "${POAPPIN_NAME}-${POAPPIN_VERSION}"
}
