#!/bin/bash

POAPPIN_NAME="blender"
POAPPIN_DESCRIPTION="Free and open source 3D creation suite"
POAPPIN_HOMEPAGE="https://www.blender.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/blender-\([0-9].*\)-windows64\.zip/\1/p'`
  # 2.83.3
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.blender.org/download/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a class=.* href="\(https.*download.*\/blender-.*-windows64\.zip\/\)".*$/\1/p' | head -n 1`
  # https://www.blender.org/download/Blender2.83/blender-2.83.3-windows64.zip/

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*\/blender-.*-windows64\.zip\)".*$/\1/p'`
  # https://ftp.halifax.rwth-aachen.de/blender/release/Blender2.83/blender-2.83.3-windows64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/http.*\/\(blender-.*-windows64\.zip\)/\1/p'`
  # blender-2.83.3-windows64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv blender-*-windows64/* .
  rm -rf blender-*
  sync
}
