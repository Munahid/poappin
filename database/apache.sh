#!/bin/bash

POAPPIN_NAME="apache"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="https://apachelounge.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/httpd-\([0-9].*\)-win64-.*\.zip/\1/p'`
  # 2.4.46
}

function poappin_app_check {
  # redirects wget calls to localhost.
  DATA=`${POAPPIN_GET_PAGE} --header="Accept: text/html" --user-agent="Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0" "https://www.apachelounge.com/download/"`

  POAPPIN_URL="https://www.apachelounge.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/download.*binaries.*httpd-.*-win64-.*\.zip\)">httpd.*\.zip<\/a>.*/\1/p'`
  # https://www.apachelounge.com/download/VS16/binaries/httpd-2.4.46-win64-VS16.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*binaries\/\(httpd-.*\.zip\)/\1/p'`
  # httpd-2.4.46-win64-VS16.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
