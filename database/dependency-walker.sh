#!/bin/bash

POAPPIN_NAME="dependency-walker"
POAPPIN_DESCRIPTION="Scans any Windows module and builds a hierarchical tree diagram of all dependent modules"
POAPPIN_HOMEPAGE="https://www.dependencywalker.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/dependency-walker-\([0-9].*\)\.zip/\1/p'`
  # 2.2.6000
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.dependencywalker.com/"`

  POAPPIN_URL="https://www.dependencywalker.com/"`echo "${DATA}" | sed -n 's/.*<a href="\(depends.*_x64\.zip\)">.*Download.*/\1/p'`
  # https://www.dependencywalker.com/depends22_x64.zip

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*Version \([0-9].*\)<\/.*<font size="2">\(\.[0-9]*\)<font.*> for x64.*/\1\2/p'`
  # 2.2.6000

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # dependency-walker-2.2.6000.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}

