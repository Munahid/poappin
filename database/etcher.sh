#!/bin/bash

POAPPIN_NAME="etcher"
POAPPIN_DESCRIPTION="Flash OS images to SD cards and USB drives"
POAPPIN_HOMEPAGE="https://www.balena.io/etcher/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^.*Portable-\([0-9].*[0-9]\)\.exe$/\1/p'`
  # 1.5.45
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.balena.io/etcher/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github.*etcher.*download.*Portable.*\.exe\)" target=.*$/\1/p'`
  # https://github.com/balena-io/etcher/releases/download/v1.5.45/balenaEtcher-Portable-1.5.45.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download\/.*\/\(balenaEtcher-Portable.*\.exe\)/\1/p'`
  # balenaEtcher-Portable-1.5.45.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  "${POAPPIN_PATH_TO_7ZIP}" x \$PLUGINSDIR/app-64.7z
  rm -rf \$PLUGINSDIR
  sync
}
