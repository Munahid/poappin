#!/bin/bash

POAPPIN_NAME="msgviewer"
POAPPIN_DESCRIPTION="E-Mail-viewer utility for .msg e-mail messages"
POAPPIN_HOMEPAGE="https://sourceforge.net/projects/msgviewer/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  POAPPIN_URL="https://sourceforge.net/projects/msgviewer/files/MSGViewer-1.9/MSGViewer-1.9.zip/download"

  # TODO: get last version first...

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/^.*files\/.*[0-9]\/\(MSGViewer-.*\.zip\)\/download$/\1/p'`
  # MSGViewer-1.9.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/MSGViewer-\([0-9].*\)\.zip/\1/p'`
  # 1.9
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv MSGViewer-1.9/* .
  rm -rf MSGViewer-1.9
}
