#!/bin/bash

POAPPIN_NAME="llvm"
POAPPIN_DESCRIPTION="The LLVM Compiler Infrastructure"
POAPPIN_HOMEPAGE="https://llvm.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/LLVM-\([0-9].*\)-win64\.exe/\1/p'`
  # 11.0.0
}

function poappin_app_check {
#  # Download page https://releases.llvm.org/download.html is not always the newest release.
#  # 15.0.3 is missing here since 11 days, last release was 15.0.2.
#  DATA=`${POAPPIN_GET_PAGE} "https://releases.llvm.org/download.html" | gunzip`
#
#  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github.*llvm-project\/releases\/download\/.*\/LLVM-.*-win64\.exe\)">Windows.*/\1/p' | head -n 1`
#  # https://github.com/llvm/llvm-project/releases/download/llvmorg-11.0.0/LLVM-11.0.0-win64.exe
#
#  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(LLVM-.*\.exe\)/\1/p'`
#  # LLVM-11.0.0-win64.exe
#
#  poappin_app_set_version

  DATA=`${POAPPIN_GET_PAGE} 'https://api.github.com/repos/llvm/llvm-project/releases/latest'`
  DATA=`echo "$DATA" | jq '. | del(.author) | { version: .name, tag: .tag_name, asset: .assets[] | select(.content_type == "application/x-msdownload") } | del(.asset.uploader) | select(.asset.name | endswith("-win64.exe"))'`

  POAPPIN_URL=`echo "$DATA" | jq '.asset.browser_download_url' | tr -d \"`
  POAPPIN_VERSION=`echo "$DATA" | jq '.version' | tr -d \" | sed -n 's/LLVM \([0-9].*\)/\1/p'`
  POAPPIN_FILENAME=`basename ${POAPPIN_URL}`
  #LLVM-15.0.3-win64.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -rf \$PLUGINSDIR
}
