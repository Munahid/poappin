#!/bin/bash

POAPPIN_NAME="tightgate-schleuse"
POAPPIN_DESCRIPTION="m-privacy TightGate-Pro Datei-Schleuse"
POAPPIN_HOMEPAGE="https://www.m-privacy.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.m-privacy.de/de/download-center/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a.*href="\(https:\/\/ftp\..*TG-Pro_Schleuse.*\.msi\)" target=.*$/\1/p'`
  # https://ftp.m-privacy.de/TG-Pro_Schleuse/TG-Pro-schleuse_3.3.2_AD_SSO_PW_win64.msi

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(TG-Pro-schleuse.*\.msi\)$/\1/p'`
  # TG-Pro-schleuse_3.3.2_AD_SSO_PW_win64.msi

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^TG-Pro-schleuse_\([0-9].*\)_AD_SSO_PW_win64\.msi$/\1/p'`
  # 3.3.2
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  local TARGET=`ls -1 "${POAPPIN_APPS_DIR}" | grep tightgate-viewer | head -n 1`
  if [ "${TARGET}" == "" ]; then
    echo "First you need an installed 'tightgate-viewer' package."
    echo "This package will install its files into the first viewer directory found."
    return
  fi

  echo "Installing the files of '${POAPPIN_NAME}' into the '${TARGET}' directory..."

  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" disk1.cab
  "${POAPPIN_PATH_TO_7ZIP}" x disk1.cab
  rm -f disk1.cab
  
  cp ./* "${POAPPIN_APPS_DIR}/${TARGET}/"
}
