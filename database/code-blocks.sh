#!/bin/bash

POAPPIN_NAME="code-blocks"
POAPPIN_DESCRIPTION="The open source, cross platform, free C, C++ and Fortran IDE"
POAPPIN_HOMEPAGE="http://www.codeblocks.org/home"
POAPPIN_LICENSE="GNU GPL v3"
POAPPIN_LICENSE_URL="https://www.gnu.org/licenses/gpl-3.0.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/codeblocks-\([0-9].*\)-nosetup\.zip/\1/p'`
  # 20.03
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "http://www.codeblocks.org/downloads/binaries"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(http.*sourceforge\.net.*Binaries.*Windows\/codeblocks-.*-nosetup\.zip\)" target=.*Sourceforge.*/\1/p' | grep -v mingw`
  # http://sourceforge.net/projects/codeblocks/files/Binaries/20.03/Windows/codeblocks-20.03-nosetup.zip
  # http://sourceforge.net/projects/codeblocks/files/Binaries/20.03/Windows/codeblocks-20.03mingw-nosetup.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*Windows\/\(codeblocks-.*-nosetup\.zip\)/\1/p'`
  # codeblocks-20.03-nosetup.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
