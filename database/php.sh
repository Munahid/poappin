#!/bin/bash

POAPPIN_NAME="php"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="https://www.php.net/"
POAPPIN_LICENSE="The PHP License 3.01"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/php-\([0-9].*\)-Win32.*\.zip/\1/p'`
  # 8.0.0
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://windows.php.net/download/"`

  POAPPIN_URL="https://windows.php.net"`echo "${DATA}" | sed -n 's/.*<a href="\(\/downloads\/releases\/php.*x64\.zip\)">Zip.*/\1/p' | grep -v "\-nts-" | head -n 1`
  # https://windows.php.net/downloads/releases/php-8.0.0-Win32-vs16-x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*releases\/\(php-.*\.zip\)/\1/p'`
  # php-8.0.0-Win32-vs16-x64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
