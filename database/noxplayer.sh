#!/bin/bash

POAPPIN_NAME="noxplayer"
POAPPIN_DESCRIPTION="The perfect Android emulator to play mobile games on PC"
POAPPIN_HOMEPAGE="https://de.bignox.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/nox_setup_v\([0-9].*\)_full_intl\.exe/\1/p'`
  # 7.0.1.6
}

function poappin_app_check {
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} "https://de.bignox.com/de/download/fullPackage?formal" 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://res06.bignox.com/full/20210923/3f360045320f4518a857f28ac36d030d.exe?filename=nox_setup_v7.0.1.6_full_intl.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*exe?filename=\(nox_setup_.*\.exe\)/\1/p'`
  # nox_setup_v7.0.1.6_full_intl.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
