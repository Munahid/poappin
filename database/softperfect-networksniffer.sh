#!/bin/bash

POAPPIN_NAME="softperfect-networksniffer"
POAPPIN_DESCRIPTION="SoftPerfect Network Protocol Analyzer is a free network sniffer for Windows"
POAPPIN_HOMEPAGE="https://www.softperfect.com/products/networksniffer/"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/softperfect-networksniffer-\([0-9].*\)\.zip/\1/p'`
  # 2.91
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.softperfect.com/products/networksniffer/"`

  POAPPIN_URL="https://www.softperfect.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/download\/freeware\/snpa_portable\.zip\)" .*Windows.*/\1/p'`
  # https://www.softperfect.com/download/freeware/snpa_portable.zip

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/[[:space:]]*\([0-9].*\) (.. .* 20..)<br>$/\1/p'`
  # 2.9.1

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # softperfect-networksniffer-2.9.1.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv 64-bit/* .
  rm -rf {32,64}-bit
}
