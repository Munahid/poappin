#!/bin/bash

POAPPIN_NAME="scan-tailor"
POAPPIN_DESCRIPTION="Interactive post-processing tool for scanned pages"
POAPPIN_HOMEPAGE="https://scantailor.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/scantailor-\([0-9].*[0-9]\)-64bit-.*\.exe/\1/p'`
  # 0.9.11.1
}

function poappin_app_check {
  # There are no current (0.9.12.1) binaries here:
  # https://github.com/scantailor/scantailor/releases/latest

  DATA=`${POAPPIN_GET_PAGE} "https://scantailor.org/downloads/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*latest version is <a href="\(https.*github\.com\/scantailor\/.*releases.*\)">[0-9].*[0-9]<\/a>.*$/\1/p'`
  # https://github.com/scantailor/scantailor/releases/tag/RELEASE_0_9_11_1

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/scantailor\/.*download.*scantailor.*64bit.*\.exe\)" rel=.*$/\1/p'`
  # https://github.com/scantailor/scantailor/releases/download/RELEASE_0_9_11_1/scantailor-0.9.11.1-64bit-install.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(scantailor-.*-64bit.*\.exe\)/\1/p'`
  # scantailor-0.9.11.1-64bit-install.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  rm -rf \$PLUGINSDIR
  rm -f Uninstaller.exe
}
