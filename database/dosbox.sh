#!/bin/bash

POAPPIN_NAME="dosbox"
POAPPIN_DESCRIPTION="DOS emulator"
POAPPIN_HOMEPAGE="https://www.dosbox.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/DOSBox\([0-9].*\)-win32-installer\.exe/\1/p' | sed 's/\-/./g'`
  # 0.74-3 -> 0.74.3
}

function poappin_app_check {
  # or from: https://sourceforge.net/projects/dosbox/files/dosbox/
  POAPPIN_URL="https://www.dosbox.com/download.php?main=1"
  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*\(https:.*\.exe\/download\)" .*>Windows.*$/\1/p'`
  # https://sourceforge.net/projects/dosbox/files/dosbox/0.74-3/DOSBox0.74-3-win32-installer.exe/download

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*dosbox\/.*\/\(DOSBox.*win32.*\.exe\)\/download$/\1/p'`
  # DOSBox0.74-3-win32-installer.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  # German language file.
  DATA=`${POAPPIN_GET_PAGE} "https://www.dosbox.com/download.php?main=1"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*dosbox\.com.*\/DOSBox-german-lang-.*\.zip\)" .*>German.*$/\1/p'`
  # https://www.dosbox.com/tools/DOSBox-german-lang-0.74.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(DOSBox-german.*\.zip\)$/\1/p'`
  # DOSBox-german-lang-0.74.zip

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -f uninstall.exe

  poappin_extract_file "$(ls -1 ${POAPPIN_DOWNLOADS_DIR}/DOSBox-german-lang-*.zip | head -n 1 | sed 's/.*\/DOSBox-german/DOSBox-german/')"
}
