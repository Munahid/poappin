#!/bin/bash

POAPPIN_NAME="lame"
POAPPIN_DESCRIPTION="High quality MPEG Audio Layer III (MP3) encoder"
POAPPIN_HOMEPAGE="https://lame.sourceforge.io/index.php"
POAPPIN_LICENSE="LGPL"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/lame\([0-9].*\)-64-20......\.zip/\1/p'`
  # 3.100
}

function poappin_app_check {
  # https://lame.sourceforge.io/download.php (only sources here)
  DATA=`${POAPPIN_GET_PAGE} "https://www.rarewares.org/mp3-lame-bundle.php"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(http.*rarewares\.org\/.*\/lame.*-64-.*\.zip\)">Download.*/\1/p'`
  # http://www.rarewares.org/files/mp3/lame3.100-64-20200409.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(lame.*\.zip\)/\1/p'`
  # lame3.100-64-20200409.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}

