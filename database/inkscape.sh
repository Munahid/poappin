#!/bin/bash

POAPPIN_NAME="inkscape"
POAPPIN_DESCRIPTION="Free vector drawing application"
POAPPIN_HOMEPAGE="https://inkscape.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://inkscape.org/release/"`

  POAPPIN_URL="https://inkscape.org"`echo "${DATA}" | sed -n 's/.*a href="\(\/release.*windows\/\)">/\1/p'`"64-bit/compressed-7z/dl/"
  # https://inkscape.org/release/1.0/windows/64-bit/compressed-7z/dl/

  DATA=`${POAPPIN_GET_PAGE} ${POAPPIN_URL}`

  POAPPIN_URL="https://inkscape.org"`echo "${DATA}" | sed -n 's/.*<meta http-equiv="Refresh".* url=\(\/.*\.7z\)".*/\1/p'`
  # https://inkscape.org/gallery/item/18471/inkscape-1.0-x64.7z (alt)
  # https://inkscape.org/gallery/item/34666/inkscape-1.2.1_2022-07-14_9c6d41e410-x64_F8IJHkW.7z

  #  <link rel="alternate" hreflang="de" href="/de/release/1.0/windows/64-bit/compressed-7z/dl/">

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(inkscape-.*\.7z\)$/\1/p'`
  # inkscape-1.2.1_2022-07-14_9c6d41e410-x64_F8IJHkW.7z

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/inkscape-\([0-9].*\)_20[0-9][0-9]-..-.._.*-x64_.*\.7z/\1/p'`
  # 1.2.1
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  # extracted directory name: inkscape-1.2.1_2022-07-14_9c6d41e410-x64
  mv inkscape-${POAPPIN_VERSION}_*/* .
  rm -rf inkscape-${POAPPIN_VERSION}_*
}
