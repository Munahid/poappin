#!/bin/bash

POAPPIN_NAME="sqlitebrowser"
POAPPIN_DESCRIPTION="DB Browser for SQLite"
POAPPIN_HOMEPAGE="https://sqlitebrowser.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # <a href="https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.12.0-win64.zip">DB Browser for SQLite - .zip (no installer) for 64-bit Windows</a>
  POAPPIN_URL=`${POAPPIN_GET_PAGE} "https://sqlitebrowser.org/dl/" | sed -n 's/^.*<a href="\(https.*-win64.zip\)">DB Browser for SQLite.*$/\1/p'`
  # https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.12.0-win64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/^.*.org\/\(DB.Browser.*\.zip\)$/\1/p'`
  # DB.Browser.for.SQLite-3.12.0-win64.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/DB.*-\([0-9].*[0-9]\)-win64\.zip$/\1/p'`
  # 3.12.0
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv DB\ Browser\ for\ SQLite/* .
  rm -rf DB\ Browser\ for\ SQLite
}
