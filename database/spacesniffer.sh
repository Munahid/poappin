#!/bin/bash

POAPPIN_NAME="spacesniffer"
POAPPIN_DESCRIPTION="Freeware disk space analyzer for Windows"
POAPPIN_HOMEPAGE="http://www.uderzo.it/main_products/space_sniffer/index.html"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # https://www.fosshub.com/SpaceSniffer.html
  # http only, fosshub wants some referer or so. Didn't work at the first try.
  DATA=`${POAPPIN_GET_PAGE} "http://www.uderzo.it/main_products/space_sniffer/download_alt.html"`

  POAPPIN_URL="http://www.uderzo.it/main_products/space_sniffer/"`echo "${DATA}" | sed -n 's/.*<a href="\(files\/spacesniffer_.*\.zip\)" title=.*$/\1/p' | head -n 1`
  # http://www.uderzo.it/main_products/space_sniffer/files/spacesniffer_1_3_0_2.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*files\/\(spacesniffer.*\.zip\)$/\1/p'`
  # spacesniffer_1_3_0_2.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/spacesniffer_\([0-9].*[0-9]\)\.zip$/\1/p' | sed 's/_/./g'`
  # 1.3.0.2
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
