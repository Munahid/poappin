#!/bin/bash

POAPPIN_NAME="7-zip"
POAPPIN_DESCRIPTION="7-Zip archive manager"
POAPPIN_HOMEPAGE="https://www.7-zip.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/7z\([0-9].*\)-x64\.exe/\1/p' | sed -n 's/\(..\)\(.*\)/\1.\2/p'`
  # 19.00
}

function poappin_app_check {
  POAPPIN_URL="https://www.7-zip.de/download.html"
  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # <strong>Download von 7-Zip 19.00 (21.02.2019) für Windows</strong>
  #POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^.*strong.*Download von 7-Zip \([0-9].*\) (..\...\.20..) für Windows.*strong.*$/\1/p' | head -1`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/^.*a class=".*" href="\(https.*-x64.exe\)">Download<\/a>.*/\1/p' | head -1`
  # https://7-zip.org/a/7z1900-x64.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/\(7z.*-x64.exe\)/\1/p'`
  # 7z1900-x64.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -f Uninstall.exe

  reg add "HKEY_CURRENT_USER\Software\7-Zip\FM" //f //v ShowGrid //t REG_DWORD //d 1
  reg add "HKEY_CURRENT_USER\Software\7-Zip\FM" //f //v FullRow //t REG_DWORD //d 1
  reg add "HKEY_CURRENT_USER\Software\7-Zip\FM" //f //v Editor //t REG_SZ //d "C:\Program Files\Notepad++\notepad++.exe"
  reg add "HKEY_CURRENT_USER\Software\7-Zip\FM" //f //v Diff //t REG_SZ //d "C:\Program Files (x86)\WinMerge\WinMergeU.exe"
}

function poappin_app_uninstall {
  reg delete "HKEY_CURRENT_USER\Software\7-Zip" //f
}
