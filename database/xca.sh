#!/bin/bash

POAPPIN_NAME="xca"
POAPPIN_DESCRIPTION="X Certificate and Key management. This application is intended for creating and managing X.509 certificates, certificate requests, RSA, DSA and EC private keys, Smartcards and CRLs. Everything that is needed for a CA is implemented."
POAPPIN_HOMEPAGE="https://hohnstaedt.de/xca/"
POAPPIN_LICENSE="BSD 3-Clause New/Revised License"
POAPPIN_LICENSE_URL="https://github.com/chris2511/xca/blob/main/COPYRIGHT"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/xca-portable-\([0-9].*\)\.zip/\1/p'`
  # 2.4.0
}

function poappin_app_check {
  # or: https://github.com/chris2511/xca/releases/
  DATA=`${POAPPIN_GET_PAGE} "https://hohnstaedt.de/xca/index.php/download"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.\/\/github\.com\/.*\/xca\/releases\/.*\/xca-portable-.*\.zip\)"><img .*$/\1/p'`
  # https://github.com/chris2511/xca/releases/download/RELEASE.2.4.0/xca-portable-2.4.0.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(xca-portable-.*\.zip\)/\1/p'`
  # xca-portable-2.4.0.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
