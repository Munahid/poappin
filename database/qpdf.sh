#!/bin/bash

POAPPIN_NAME="qpdf"
POAPPIN_DESCRIPTION="A Content-Preserving PDF Transformation System"
POAPPIN_HOMEPAGE="https://sourceforge.net/projects/qpdf/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # https://sourceforge.net/projects/qpdf/files/
  POAPPIN_URL="https://github.com/qpdf/qpdf/releases"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # <a href="/qpdf/qpdf/releases/download/release-qpdf-10.0.1/qpdf-10.0.1-bin-mingw64.zip" rel="nofollow" class="...">
  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*a href="\(\/qpdf\/.*bin-mingw64\.zip\)" rel=.*$/\1/p' | head -n 1`
  # https://github.com/qpdf/qpdf/releases/download/release-qpdf-10.0.1/qpdf-10.0.1-bin-mingw64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(qpdf-[0-9].*[0-9]-bin-mingw64\.zip\)$/\1/p'`
  # qpdf-10.0.1-bin-mingw64.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^qpdf-\([0-9].*[0-9]\)-bin-mingw64\.zip$/\1/p'`
  # 10.0.1
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv qpdf*/* .
  rm -rf qpdf*
}

# TODO: add path: apps/qpdf-10.0.1/bin
