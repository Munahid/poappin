#!/bin/bash

POAPPIN_NAME="shotcut"
POAPPIN_DESCRIPTION="free, open source, cross-platform video editor"
POAPPIN_HOMEPAGE="https://www.shotcutapp.com/"
POAPPIN_LICENSE="GNU GPL 3"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/shotcut-win64-\([0-9].*\)\.zip/\1/p'`
  # 210920
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.shotcutapp.com/download/"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github\.com\/mltframework\/shotcut\/releases\/.*\/shotcut-win64-.*\.zip\)">Windows portable.*/\1/p'`
  # https://github.com/mltframework/shotcut/releases/download/v21.09.20/shotcut-win64-210920.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download.*\/\(shotcut-win64-.*\.zip\)/\1/p'`
  # shotcut-win64-210920.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv Shotcut/* .
  sync
  rm -rf Shotcut
  sync
}
