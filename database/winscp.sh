#!/bin/bash

POAPPIN_NAME="winscp"
POAPPIN_DESCRIPTION="Popular SFTP client and FTP client for Microsoft Windows"
POAPPIN_HOMEPAGE="https://winscp.net/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/WinSCP-\([0-9].*\)-Portable\.zip/\1/p'`
  # 5.17.7
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://winscp.net/eng/downloads.php"`

  POAPPIN_URL="https://winscp.net"`echo "${DATA}" | sed -n 's/.*<a href="\(\/download\/WinSCP-.*-Portable\.zip\)" .*>Download.*/\1/p' | head -n 1`
  # https://winscp.net/download/WinSCP-5.17.7-Portable.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(WinSCP-.*\.zip\)/\1/p'`
  # WinSCP-5.17.7-Portable.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  # German language file.
  DATA=`${POAPPIN_GET_PAGE} "https://winscp.net/eng/translations.php"`

  POAPPIN_URL="https://winscp.net/"`echo "${DATA}" | grep -v beta | sed -n 's/.*<a href="\.\.\/\(translations\/.*\/de\.zip\)">[0-9].*<\/a>$/\1/p'`
  # https://winscp.net/translations/dll/5.17.7/de.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/dll\/\([0-9].*\)\/de\.zip$/WinSCP-de-\1.zip/p'`
  # de.zip -> WinSCP-de-5.17.7.zip

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  poappin_extract_file "$(ls -1 ${POAPPIN_DOWNLOADS_DIR}/WinSCP-de-*.zip | head -n 1 | sed 's/.*\/WinSCP-de/WinSCP-de/')"
}
