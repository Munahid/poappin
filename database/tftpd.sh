#!/bin/bash

POAPPIN_NAME="tftpd"
POAPPIN_DESCRIPTION="Tftpd64 is a free, opensource IPv6 ready application which includes DHCP, TFTP, DNS, SNTP and Syslog servers as well as a TFTP client."
POAPPIN_HOMEPAGE="https://tftpd32.jounin.net/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/tftpd64\.\([0-9].*\)\.zip/\1/p'`
  # 464
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://tftpd32.jounin.net/tftpd32_download.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https...bitbucket\.org.*\/tftpd64\..*\.zip\)">.*/\1/p' | head -n 1`
  # https://bitbucket.org/phjounin/tftpd64/downloads/tftpd64.464.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(tftpd64\..*\.zip\)/\1/p'`
  # tftpd64.464.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
