#!/bin/bash

POAPPIN_NAME="windjview"
POAPPIN_DESCRIPTION="Fast, compact and powerful DjVu viewer for Windows"
POAPPIN_HOMEPAGE="https://windjview.sourceforge.io/"
POAPPIN_LICENSE="GNU GPL v2+"
POAPPIN_LICENSE_URL="http://www.gnu.org/copyleft/gpl.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/WinDjView-\([0-9].*\)-.*\.exe/\1/p'`
  # 2.1
}

function poappin_app_check {
  # Download from: https://sourceforge.net/projects/windjview/files/WinDjView/
  # We use the provided "lastest" files. There is only one, so it is always
  # a matching windows version (not linux and also not a separate 32 bit).

  POAPPIN_URL=`${POAPPIN_GET_HEADERS} "https://sourceforge.net/projects/windjview/files/latest/download" 2>&1 | sed -n 's/.*Location: \(https.*\)$/\1/p' | head -n 1`
  # https://downloads.sourceforge.net/project/windjview/WinDjView/2.1/
  # WinDjView-2.1-Setup.exe?r=&ts=1594216536&use_mirror=deac-riga

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/\(WinDjView-.*\.exe\).*$/\1/p'`
  # WinDjView-2.1-Setup.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  # Extract with -aou -> aUto rename extracting files
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm uninstall.exe WinDjViewRU.dll

  # There are two files with the same name. Auto-renamed.
  # Find the x64 file and delete the other one.

  if [ -f "WinDjView.exe" ] && [ -f "WinDjView_1.exe" ] ; then
    echo "Both exe files found."

    if file "WinDjView.exe" | grep -q "Intel 80386" && file "WinDjView_1.exe" | grep -q "x86-64" ; then
      echo "Moving the 64 bit version over the 32 bit version."
      mv "WinDjView_1.exe" "WinDjView.exe"
    else
      echo "Removing the 32 bit version."
      rm "WinDjView_1.exe"
    fi
  else
    echo "Files not found: WinDjView*.exe."
  fi

  reg add "HKEY_CURRENT_USER\Software\Andrew Zhezherun\WinDjView\Settings" //f //v check-updates //t REG_DWORD //d 0
  reg add "HKEY_CURRENT_USER\Software\Andrew Zhezherun\WinDjView\Settings" //f //v warn-not-default-viewer //t REG_DWORD //d 0
  # 1031 de-de, 1033 en-us
  reg add "HKEY_CURRENT_USER\Software\Andrew Zhezherun\WinDjView\Settings" //f //v language //t REG_DWORD //d 1031
}

function poappin_app_uninstall {
  reg delete "HKEY_CURRENT_USER\Software\Andrew Zhezherun\WinDjView" //f
}
