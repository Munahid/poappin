#!/bin/bash

POAPPIN_NAME="python"
POAPPIN_DESCRIPTION="Python programming language"
POAPPIN_HOMEPAGE="https://www.python.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  POAPPIN_URL="https://www.python.org/downloads/windows/"
  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # <a href="/downloads/release/python-383/">Latest Python 3 Release - Python 3.8.3</a>
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^.*a href="\/downloads\/.*">Latest Python 3 Release - Python \([0-9].*[0-9]\)<\/a>.*$/\1/p'`
  # 3.8.3

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a href="\(https:.*\/python-3.*embed-amd64\.zip\)">.*$/\1/p' | grep "${POAPPIN_VERSION}-"`
  # https://www.python.org/ftp/python/3.8.3/python-3.8.3-embed-amd64.zip
  # The "-" after the version string (grep) filters alphas, betas und release candidates.

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(python-3.*\.zip\)$/\1/p'`
  # python-3.8.3-embed-amd64.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
