#!/bin/bash

POAPPIN_NAME="free-commander"
POAPPIN_DESCRIPTION="Easy-to-use alternative to the standard windows file manager"
POAPPIN_HOMEPAGE="https://freecommander.com/en/summary/"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL="https://freecommander.com/en/license/"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Free.*portable-\([0-9].*\)\.zip/\1/p'`
  # 810a
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://freecommander.com/en/downloads-portable/"`

  POAPPIN_URL="https://freecommander.com"`echo "${DATA}" | sed -n 's/.*<a .* href="\(\/downloads\/FreeCommanderXE.*portable\.zip\)">.*$/\1/p'`
  # https://freecommander.com/downloads/FreeCommanderXE-32-public_portable.zip

  # <span style="color: #ffffff;">FreeCommander XE 2020 Build 810a portable ZIP</span></strong></p>
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<span style=.*>FreeCommander XE 2020 Build \([0-9].*\) portable ZIP<\/span>.*$/\1/p'`
  # 810a

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/downloads\/\(FreeCommander.*\)\.zip$/\1/p'`"-${POAPPIN_VERSION}.zip"
  # FreeCommanderXE-32-public_portable-810a.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
