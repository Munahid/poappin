#!/bin/bash

POAPPIN_NAME="gspot"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="http://gspot.headbands.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/gspot-\([0-9].*\)\.zip/\1/p'`
  # 2.70a
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "http://gspot.headbands.com/"`

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<a href=.*>Current version: GSpot v\([0-9].*\)<\/a>.*/\1/p'`
  # 2.70a

  POAPPIN_URL="http://gspot.headbands.com/"`echo "${DATA}" | sed -n 's/.*<a href="\(v.*\.htm\)">Current version: GSpot.*/\1/p'`
  # http://gspot.headbands.com/v26x/index.htm

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL="${POAPPIN_URL/index.htm/}"`echo "${DATA}" | sed -n 's/.*<a href="\(GSpot.*\.zip\)">.*/\1/p' | head -n 1`
  # http://gspot.headbands.com/v26x/GSpot270a.zip

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # gspot-2.70a.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}

function poappin_app_uninstall {
  reg delete "HKEY_CURRENT_USER\Software\GSpot Appliance Corp\GSpot" //f
}
