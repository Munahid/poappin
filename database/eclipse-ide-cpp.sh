#!/bin/bash

POAPPIN_NAME="eclipse-ide-cpp"
POAPPIN_DESCRIPTION="Eclipse IDE for C/C++ Developers"
POAPPIN_HOMEPAGE="https://www.eclipse.org/eclipseide/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/eclipse-cpp-\([0-9].*\)-win32-x86_64\.zip/\1/p' | sed 's/-/./g'`
  # 2020.12.R
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.eclipse.org/downloads/packages/"`

  POAPPIN_URL="https:"`echo "${DATA}" | sed -n "s/.*<a href='\(\/\/www\.eclipse\.org.*eclipse-cpp-.*x86_64\.zip\)'>x86_64.*/\1/p"`
  # https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-12/R/eclipse-cpp-2020-12-R-win32-x86_64.zip

  # select a mirror page
  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL="https://www.eclipse.org/downloads/"`echo "${DATA}" | sed -n 's/.*<a href="\(download\.php.*eclipse-cpp-.*x86_64\.zip.r=1\)">Direct link to file.*/\1/p'`
  # https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-12/R/eclipse-cpp-2020-12-R-win32-x86_64.zip&r=1

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*release.*\/\(eclipse-cpp.*\.zip\).*/\1/p'`
  # eclipse-cpp-2020-12-R-win32-x86_64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv eclipse/* .
  mv eclipse/.eclipseproduct .
  rm -rf eclipse
}
