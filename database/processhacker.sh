#!/bin/bash

POAPPIN_NAME="processhacker"
POAPPIN_DESCRIPTION="Powerful, multi-purpose tool that helps you monitor system resources, debug software and detect malware"
POAPPIN_HOMEPAGE="https://processhacker.sourceforge.io/"
POAPPIN_LICENSE="GNU GPL v3"
POAPPIN_LICENSE_URL="https://processhacker.sourceforge.io/gpl.php"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/processhacker-\([0-9].*\)-bin\.zip/\1/p'`
  # 2.39
}

function poappin_app_check {
  # https://processhacker.sourceforge.io/downloads.php
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/processhacker/processhacker/releases/latest"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*a href="\(\/.*download.*processhacker-.*-bin\.zip\)".*/\1/p'`
  # https://github.com/processhacker/processhacker/releases/download/v2.39/processhacker-2.39-bin.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(processhacker-.*\.zip\)/\1/p'`
  # processhacker-2.39-bin.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  rm -rf x86
  mv x64/* .
  rm -rf x64
}

