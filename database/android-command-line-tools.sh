#!/bin/bash

POAPPIN_NAME="android-command-line-tools"
POAPPIN_DESCRIPTION="Android SDK command line tools (from Android Studio)"
POAPPIN_HOMEPAGE="https://developer.android.com/studio"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/commandlinetools-win-\([0-9].*[0-9]\)_latest\.zip$/\1/p'`
  # 6609375
}

function poappin_app_check {
  POAPPIN_URL="https://developer.android.com/studio"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL="https://dl.google.com/android/repository/"`echo "${DATA}" | sed -n 's/.*>\(commandlinetools-win.*\.zip\)<\/button>$/\1/p'`
  # https://dl.google.com/android/repository/commandlinetools-win-6609375_latest.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/\(commandlinetools-win-.*_latest\.zip\)$/\1/p'`
  # commandlinetools-win-6609375_latest.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv tools/* .
  rm -rf tools
}
