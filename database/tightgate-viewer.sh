#!/bin/bash

POAPPIN_NAME="tightgate-viewer"
POAPPIN_DESCRIPTION="m-privacy TightGate-Pro VNC-Viewer"
POAPPIN_HOMEPAGE="https://www.m-privacy.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.m-privacy.de/de/download-center/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a.*href="\(https:\/\/ftp\..*TG-Pro_Viewer\/windows\/.*win64-all.*\.msi\)" target=.*$/\1/p'`
  # https://ftp.m-privacy.de/TG-Pro_Viewer/windows/TG-Pro-vnc_3.4.2_win64-all.msi

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*windows\/\(TG-Pro-vnc.*\.msi\)$/\1/p'`
  # TG-Pro-vnc_3.4.2_win64-all.msi

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^.*-vnc_\([0-9].*\)_win64.*$/\1/p'`
  # 3.4.2
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" disk1.cab
  "${POAPPIN_PATH_TO_7ZIP}" x disk1.cab
  rm -f disk1.cab

  mkdir -p gstreamer/x86_64/bin
  mkdir -p gstreamer/x86_64/lib/gstreamer-1.0
  mkdir pulseaudio

  mv gstlaunch1.0.exe            gstreamer/x86_64/bin/gst-launch-1.0.exe
  mv libffi7.dll                 gstreamer/x86_64/bin/libffi-7.dll
  mv libgio2.00.dll              gstreamer/x86_64/bin/libgio-2.0-0.dll
  mv libglib2.00.dll             gstreamer/x86_64/bin/libglib-2.0-0.dll
  mv libgmodule2.00.dll          gstreamer/x86_64/bin/libgmodule-2.0-0.dll
  mv libgobject2.00.dll          gstreamer/x86_64/bin/libgobject-2.0-0.dll
  mv libgstaudio1.00.dll         gstreamer/x86_64/bin/libgstaudio-1.0-0.dll
  mv libgstbase1.00.dll          gstreamer/x86_64/bin/libgstbase-1.0-0.dll
  mv libgstnet1.00.dll           gstreamer/x86_64/bin/libgstnet-1.0-0.dll
  mv libgstpbutils1.00.dll       gstreamer/x86_64/bin/libgstpbutils-1.0-0.dll
  mv libgstreamer1.00.dll        gstreamer/x86_64/bin/libgstreamer-1.0-0.dll
  mv libgstrtp1.00.dll           gstreamer/x86_64/bin/libgstrtp-1.0-0.dll
  mv libgstrtsp1.00.dll          gstreamer/x86_64/bin/libgstrtsp-1.0-0.dll
  mv libgstrtspserver1.00.dll    gstreamer/x86_64/bin/libgstrtspserver-1.0-0.dll
  mv libgstsctp1.00.dll          gstreamer/x86_64/bin/libgstsctp-1.0-0.dll
  mv libgstsdp1.00.dll           gstreamer/x86_64/bin/libgstsdp-1.0-0.dll
  mv libgsttag1.00.dll           gstreamer/x86_64/bin/libgsttag-1.0-0.dll
  mv libgstvideo1.00.dll         gstreamer/x86_64/bin/libgstvideo-1.0-0.dll
  mv libintl8.dll                gstreamer/x86_64/bin/libintl-8.dll
  mv libjpeg8.dll                gstreamer/x86_64/bin/libjpeg-8.dll
  mv libopenjp2.dll              gstreamer/x86_64/bin/libopenjp2.dll
  mv liborc0.40.dll              gstreamer/x86_64/bin/liborc-0.4-0.dll
  mv libwinpthread1.dll          gstreamer/x86_64/bin/libwinpthread-1.dll
  mv libz1.dll                   gstreamer/x86_64/bin/libz-1.dll 
  mv libgstcoreelements.dll      gstreamer/x86_64/lib/gstreamer-1.0/libgstcoreelements.dll
  mv libgstjpeg.dll              gstreamer/x86_64/lib/gstreamer-1.0/libgstjpeg.dll
  mv libgstrtp.dll               gstreamer/x86_64/lib/gstreamer-1.0/libgstrtp.dll
  mv libgstudp.dll               gstreamer/x86_64/lib/gstreamer-1.0/libgstudp.dll
  mv libgstwinks.dll             gstreamer/x86_64/lib/gstreamer-1.0/libgstwinks.dll
  mv libjsonc2.dll               pulseaudio/libjson-c-2.dll
  mv libltdl7.dll                pulseaudio/libltdl-7.dll
  mv libprotocolnative.dll       pulseaudio/libprotocol-native.dll
  mv libpulse0.dll               pulseaudio/libpulse-0.dll
  mv libpulsecommon6.0.dll       pulseaudio/libpulsecommon-6.0.dll
  mv libpulsecore6.0.dll         pulseaudio/libpulsecore-6.0.dll
  mv libsndfile1.dll             pulseaudio/libsndfile-1.dll
  mv modulenativeprotocoltcp.dll pulseaudio/module-native-protocol-tcp.dll
  mv modulewaveout.dll           pulseaudio/module-waveout.dll
  mv pulseaudio.exe              pulseaudio/pulseaudio.exe
}
