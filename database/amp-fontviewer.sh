#!/bin/bash

POAPPIN_NAME="amp-fontviewer"
POAPPIN_DESCRIPTION="Powerful font manager"
POAPPIN_HOMEPAGE="https://www.ampsoft.net/utilities/FontViewer.php"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/amp-fontviewer-\([0-9].*\)\.zip/\1/p'`
  # 3.86
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.ampsoft.net/utilities/FontViewer.php"`

  POAPPIN_URL="https://www.ampsoft.net/"`echo "${DATA}" | sed -n 's/.*a href="\(\/files\/FontViewer\.zip\)">ZIP.*/\1/p'`
  # https://www.ampsoft.net/files/FontViewer.zip

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<h1>AMP Font Viewer \([0-9].*\)<\/h1>.*/\1/p'`
  # 3.86

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # amp-fontviewer-3.86.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}

