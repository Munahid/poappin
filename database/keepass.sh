#!/bin/bash

POAPPIN_NAME="keepass"
POAPPIN_DESCRIPTION="KeePass Password Safe"
POAPPIN_HOMEPAGE="https://keepass.info/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://keepass.info/download.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/<a href="\(https.*\.zip\/download\)"$/\1/p' | head -n 1`
  # https://sourceforge.net/projects/keepass/files/KeePass%202.x/2.45/KeePass-2.45.zip/download

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*files.*\/\(KeePass-[0-9].*[0-9]\.zip\)\/download$/\1/p'`
  # KeePass-2.45.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^KeePass-\([0-9].*[0-9]\)\.zip$/\1/p'`
  # 2.45
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  # Download latest german language file.
  DATA=`${POAPPIN_GET_PAGE} "https://keepass.info/translations.html"`
  URL=`echo "${DATA}" | sed -n 's/^.*<a href="\(https.*KeePass-2.*-German\.zip\)" .*$/\1/p'`
  # https://downloads.sourceforge.net/keepass/KeePass-2.45-German.zip
  FILE=`echo "${URL}" | sed -n 's/^https.*\(KeePass-2.*-German\.zip\)$/\1/p'`
  # KeePass-2.45-German.zip
  if [ ! -f "${POAPPIN_DOWNLOADS_DIR}/${FILE}" ]; then
    ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${FILE}" "${URL}"
  fi

  # Download latest 'keepassrpc' plugin.
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/kee-org/keepassrpc/releases/latest"`
  URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/kee-org\/keepassrpc\/releases\/.*\.plgx\)" .*$/\1/p'`
  # https://github.com/kee-org/keepassrpc/releases/download/v1.12.1/KeePassRPC.plgx
  VERSION=`echo "${DATA}" | sed -n 's/.*<a href=".*\/download\/v\([0-9].*\)\/KeePassRPC\.plgx".*$/\1/p'`
  # 1.12.1
  FILE="KeePassRPC-${VERSION}.plgx"
  if [ ! -f "${POAPPIN_DOWNLOADS_DIR}/${FILE}" ]; then
    ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${FILE}" "${URL}"
  fi
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  poappin_extract_file "$(ls -1 ${POAPPIN_DOWNLOADS_DIR}/KeePass-*-German.zip | tail -n 1 | sed 's/.*\/KeePass/KeePass/g')"
  cp "$(ls -1 ${POAPPIN_DOWNLOADS_DIR}/KeePassRPC-*.plgx | tail -n 1)" Plugins/
  mv German.lngx Languages/
}
