#!/bin/bash

POAPPIN_NAME="zekr"
POAPPIN_DESCRIPTION="Open source Quran study software"
POAPPIN_HOMEPAGE="https://sourceforge.net/projects/zekr/"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# not working homepage: https://zekr.org/

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://sourceforge.net/projects/zekr/files/Zekr/"`

  # first is best?  
  POAPPIN_URL="https://sourceforge.net"`echo "${DATA}" | sed -n 's/.*<a href="\(\/projects\/zekr\/files\/Zekr\/.*\)".*$/\1/p' | head -n 1`
  # https://sourceforge.net/projects/zekr/files/Zekr/zekr-1.1.0/

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # There are two portable archives, with and without jre. We use the jre one.
  # It contains a jre directory and the bat and ini files are slightly different.
  # Later we may change to the other version and use an up-to-date jre.
  # https://sourceforge.net/projects/zekr/files/Zekr/zekr-1.1.0/zekr-1.1.0-windows-portable-jre.zip/download
  # https://sourceforge.net/projects/zekr/files/Zekr/zekr-1.1.0/zekr-1.1.0-windows-portable.zip/download

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*\/projects\/zekr\/files\/Zekr\/.*windows-portable-jre\.zip\/download\)"$/\1/p'`

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*files\/.*\/\(zekr-.*-windows-port.*\.zip\)\/download$/\1/p'`
  # zekr-1.1.0-windows-portable-jre.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/zekr-\([0-9].*[0-9]\)-windows.*\.zip$/\1/p'`
  # 1.1.0
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  # Offline recitations:

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/zekr-sudais-64kbps-offline.recit.zip" \
    "http://sourceforge.net/projects/zekr/files/recit/sudais-64kbps-offline.recit.zip/download"

  # German translations from: http://tanzil.net/trans/zekr.php

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/zekr-de.aburida.trans.zip" "http://tanzil.net/trans/de.aburida.trans.zip"
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/zekr-de.bubenheim.trans.zip" "http://tanzil.net/trans/de.bubenheim.trans.zip"
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/zekr-de.khoury.trans.zip" "http://tanzil.net/trans/de.khoury.trans.zip"
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/zekr-de.zaidan.trans.zip" "http://tanzil.net/trans/de.zaidan.trans.zip"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  mv zekr/* .
  sync
  rm -rf zekr
  sync

  # http://zekr.org/resources.html

  # Add recitations.

  "${POAPPIN_PATH_TO_7ZIP}" x ${POAPPIN_DOWNLOADS_DIR}/zekr-sudais-64kbps-offline.recit.zip
  mv sudais-64kbps-offline res/audio/
  mv recitation.properties res/audio/sudais-64kbps-offline.properties

  # Add translations.

  cp ${POAPPIN_DOWNLOADS_DIR}/zekr-de.aburida.trans.zip   res/text/trans/de.aburida.trans.zip
  cp ${POAPPIN_DOWNLOADS_DIR}/zekr-de.bubenheim.trans.zip res/text/trans/de.bubenheim.trans.zip
  cp ${POAPPIN_DOWNLOADS_DIR}/zekr-de.khoury.trans.zip    res/text/trans/de.khoury.trans.zip
  cp ${POAPPIN_DOWNLOADS_DIR}/zekr-de.zaidan.trans.zip    res/text/trans/de.zaidan.trans.zip

  echo "User data is in %USERPROFILE%\.zekr, default config in res/config/"
  # network proxy here too

  # also change $USERPROFILE/.zekr/config/config.properties?
  # here I do: rm -rf $USERPROFILE/.zekr
  sed -i 's/lang.default = .*/lang.default = de_DE/' res/config/config.properties
  sed -i 's/trans.name.mode = .*/trans.name.mode = localized/' res/config/config.properties
  sed -i 's/trans.default = .*/trans.default = de.bubenheim/' res/config/config.properties
  sed -i 's/audio.default = .*/audio.default = sudais-64kbps-offline/' res/config/config.properties
  sed -i 's/audio.volume = .*/audio.volume = 40/' res/config/config.properties
  sed -i 's/audio.lookupMode = .*/audio.lookupMode = offline-online/' res/config/config.properties
  sed -i 's/update.enable = .*/update.enable = false/' res/config/config.properties
  sed -i 's/update.enableMenu = .*/update.enableMenu = false/' res/config/config.properties
}
