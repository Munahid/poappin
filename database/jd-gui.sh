#!/bin/bash

POAPPIN_NAME="jd-gui"
POAPPIN_DESCRIPTION="Standalone graphical utility that displays Java source codes"
POAPPIN_HOMEPAGE="https://java-decompiler.github.io/"
POAPPIN_LICENSE="GNU GPL v3"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/jd-gui-windows-\([0-9].*\)\.zip/\1/p'`
  # 1.6.6
}

function poappin_app_check {
  # https://github.com/java-decompiler/jd-gui/releases
  DATA=`${POAPPIN_GET_PAGE} "https://java-decompiler.github.io/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(.*\/jd-gui-windows-[0-9].*\.zip\)">.*jd-gui.*$/\1/p'`
  # https://github.com/java-decompiler/jd-gui/releases/download/v1.6.6/jd-gui-windows-1.6.6.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(jd-gui-windows-.*\.zip\)/\1/p'`
  # jd-gui-windows-1.6.6.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv jd-gui-windows-*/* .
  rm -rf jd-gui-windows-*/
}

