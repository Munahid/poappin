#!/bin/bash

POAPPIN_NAME="winmail-opener"
POAPPIN_DESCRIPTION="Opens winmail.dat files"
POAPPIN_HOMEPAGE="https://www.eolsoft.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.eolsoft.com/download/"`

  # <a target="_blank" href="http://download.cnet.com/Winmail-Opener/3000-2369_4-10469892.html?part=dl-&amp;subj=dl&amp;tag=button" title="Download Winmail Opener">Winmail Opener<br> (version: 1.6, last update: 28 May 2017</a> (~355 KB)
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^.*a target=.* href="http.*download\.cnet\.com\/Winmail-Opener.*">Winmail Opener.* (version: \([0-9].*[0-9]\)\, last update.*$/\1/p' | head -1`
  # 1.6

  # <div style="font-size: 70%"><a href="/download/winmail_opener.exe">Direct link (without Download.com installer)</a></div>
  POAPPIN_URL="${POAPPIN_URL//\/download\//}"`echo "${DATA}" | sed -n 's/^.*div style=.*a href="\(\/download\/.*\.exe\)">Direct link.*$/\1/p' | head -1`
  # https://www.eolsoft.com/download/winmail_opener.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/download\/\(.*\.exe\)/\1/p'`
  # winmail_opener.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  rm uninst.exe
  rm -rf \$PLUGINSDIR
}
