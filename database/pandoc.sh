#!/bin/bash

POAPPIN_NAME="pandoc"
POAPPIN_DESCRIPTION="Converts files from one markup format into another"
POAPPIN_HOMEPAGE="https://pandoc.org/"
POAPPIN_LICENSE="GNU GPL"
POAPPIN_LICENSE_URL="http://www.gnu.org/copyleft/gpl.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/pandoc-\([0-9].*[0-9]\)-windows.*\.zip$/\1/p'`
  # 2.10.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/jgm/pandoc/releases/latest"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*a href="\(\/jgm\/pandoc\/releases.*pandoc-.*-windows-x86_64\.zip\)" rel=.*$/\1/p'`
  # https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-windows-x86_64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download\/.*\/\(pandoc-.*-windows.*\.zip\)/\1/p'`
  # pandoc-2.10.1-windows-x86_64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  mv ${POAPPIN_NAME}-${POAPPIN_VERSION}/* .
  rm -rf "${POAPPIN_NAME}-${POAPPIN_VERSION}"
  sync
}
