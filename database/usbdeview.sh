#!/bin/bash

POAPPIN_NAME="usbdeview"
POAPPIN_DESCRIPTION="Lists all USB devices that are currently or where previously connected to your computer"
POAPPIN_HOMEPAGE="https://www.nirsoft.net/utils/usb_devices_view.html"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/'${POAPPIN_NAME}'-\([0-9].*\)\.zip/\1/p'`
  # 3.01
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.nirsoft.net/utils/usb_devices_view.html"`

  POAPPIN_URL="https://www.nirsoft.net/utils/"`echo "${DATA}" | sed -n 's/^<a class=.* href="\(usbdeview.*x64\.zip\)">Download USBDeview for x64 systems<\/a>$/\1/p'`
  # https://www.nirsoft.net/utils/usbdeview-x64.zip

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^<td>USBDeview v\([0-9].*\)$/\1/p'`
  # 3.01

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # usbdeview-3.01.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_NAME}-DE.zip" \
    "https://www.nirsoft.net/utils/trans/usbdeview_german.zip"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  "${POAPPIN_PATH_TO_7ZIP}" x -aou -y "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_NAME}-DE.zip"
}

