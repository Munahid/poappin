#!/bin/bash

POAPPIN_NAME="wix"
POAPPIN_DESCRIPTION="Windows Installer XML (WiX) Toolset"
POAPPIN_HOMEPAGE="https://wixtoolset.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/wix-\([0-9].*\)\.zip/\1/p'`
  # 3.11.2
}

function poappin_app_check {
#  POAPPIN_URL="https://wixtoolset.org/releases/"
#
#  # <p><a class="btn btn-primary" href="/releases/v3.11.2/stable">Download WiX v3.11.2</a></p>
#  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`
#  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^.*<a class="btn btn-primary" href=".*">Download WiX v\([0-9].*\)<\/a>.*$/\1/p'`
#  # 3.11.2
#
#  POAPPIN_URL=`echo "${DATA}" | sed -n 's/^.*<a class="btn btn-primary" href="\(.*\)">Download WiX v.*$/https:\/\/wixtoolset.org\1/p'`
#  # https://wixtoolset.org/releases/v3.11.2/stable
#
#  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`
#
#  # redirects to: https://github.com/wixtoolset/wix3/releases/tag/wix3112rtm
#  # <a href="/wixtoolset/wix3/releases/download/wix3112rtm/wix311-binaries.zip" rel="nofollow" class="d-flex...
#  POAPPIN_URL=`echo "${DATA}" | sed -n 's/^.*<a href="\(\/wixtoolset\/.*-binaries\.zip\)" rel=.*$/https:\/\/github.com\1/p'`
#  # https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311-binaries.zip
#
#  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"

  DATA=`${POAPPIN_GET_PAGE} 'https://api.github.com/repos/wixtoolset/wix3/releases/latest'`
  DATA=`echo "$DATA" | jq '. | del(.author) | { version: .name, tag: .tag_name, asset: .assets[] | select(.content_type == "application/x-zip-compressed") } | del(.asset.uploader) | select(.asset.name | endswith("-binaries.zip"))'`

  POAPPIN_URL=`echo "$DATA" | jq '.asset.browser_download_url' | tr -d \"`
  POAPPIN_VERSION=`echo "$DATA" | jq '.version' | tr -d \" | sed -n 's/.*WiX Toolset v\([0-9].*\)/\1/p'`
  POAPPIN_FILENAME=`basename ${POAPPIN_URL}`
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "$POAPPIN_URL"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
