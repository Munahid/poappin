#!/bin/bash

POAPPIN_NAME="amp-winoff"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="https://www.ampsoft.net/utilities/WinOFF.php"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/amp-winoff-\([0-9].*\)\.zip/\1/p'`
  # 5.0.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.ampsoft.net/utilities/WinOFF.php"`

  POAPPIN_URL="https://www.ampsoft.net/"`echo "${DATA}" | sed -n 's/.*a href="\(\/files\/WinOFF\.zip\)">ZIP.*/\1/p'`
  # https://www.ampsoft.net/files/WinOFF.zip

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<h1>AMP WinOFF \([0-9].*\)<\/h1>.*/\1/p'`
  # 5.0.1

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # amp-winoff-5.0.1.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}

