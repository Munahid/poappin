#!/bin/bash

POAPPIN_NAME="volume2"
POAPPIN_DESCRIPTION="Advanced Windows volume control"
POAPPIN_HOMEPAGE="https://www.deviantart.com/irzyxa"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://github.com/irzyxa/Volume2 - bug reports
# https://volumesqr.at.ua/ - files here, only old files shown

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Volume2-\([0-9].*\)\.zip/\1/p'`
  # 1.1.5.404
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://irzyxa.blogspot.com/p/downloads.html"`

  # <a href="http://volumesqr.at.ua/Beta/Volume2_1_1_6.zip" style="color: #0069d2; text-decoration-line: none;">volumesqr.at.ua
  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(http.*at\.ua\/Release\/Volume2.*\.zip\)" style=.*/\1/p'  | head -n 1`
  # http://volumesqr.at.ua/Release/Volume2_1_1_5_404.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(Volume2.*\.zip\)/\1/p' | sed 's/Volume2_/Volume2-/g' | sed 's/_/./g'`
  # Volume2-1.1.5.404.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv Volume2/* .
  rm -rf Volume2
}
