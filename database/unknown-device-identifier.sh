#!/bin/bash

POAPPIN_NAME="unknown-device-identifier"
POAPPIN_DESCRIPTION="Enables you to identify the yellow question mark labeled Unknown Devices in Device Manager"
POAPPIN_HOMEPAGE="http://www.zhangduo.com/udi.html"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/'${POAPPIN_NAME}'-\([0-9].*\)\.exe/\1/p'`
  # 9.01
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "http://www.zhangduo.com/udi.html"`

  POAPPIN_URL="http://www.zhangduo.com/"`echo "${DATA}" | sed -n 's/.*<a href="\(UnknownDeviceIdentifier\.exe\)">Download Now.*/\1/p'`
  # http://www.zhangduo.com/UnknownDeviceIdentifier.exe

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*Unknown Device Identifier v\([0-9].*\).*/\1/p'`
  # 9.01

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.exe"
  # unknown-device-identifier-9.01.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #poappin_extract_inno_setup_file "${POAPPIN_FILENAME}"

  ../innounp-0.49/innounp.exe -x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv \{app\}/* .
  rm -rf \{app\}
}
