#!/bin/bash

POAPPIN_NAME="rufus"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="https://rufus.ie/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/rufus-\([0-9].*\)\.exe/\1/p'`
  # 3.13p
}

function poappin_app_check {
  # index page redirects via javascript.
  DATA=`${POAPPIN_GET_PAGE} "https://rufus.ie/en_IE.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github.*rufus.*download.*\/rufus-.*\.exe\)">Rufus .* Portable<\/a>.*/\1/p'`
  # https://github.com/pbatard/rufus/releases/download/v3.13/rufus-3.13p.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(rufus-.*\.exe\)/\1/p'`
  # rufus-3.13p.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" .
}
