#!/bin/bash

POAPPIN_NAME="mediainfo"
POAPPIN_DESCRIPTION="Display of the most relevant technical and tag data for video and audio files"
POAPPIN_HOMEPAGE="https://mediaarea.net/en/MediaInfo"
POAPPIN_LICENSE="BSD-style"
POAPPIN_LICENSE_URL="https://mediaarea.net/en/MediaInfo/License"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/MediaInfo_GUI_\([0-9].*\)_Windows.*\.7z/\1/p'`
  # 20.08
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://mediaarea.net/en/MediaInfo/Download/Windows"`

  POAPPIN_URL="https:"`echo "${DATA}" | sed -n 's/.*<a href="\(\/\/mediaarea.*download.*\/MediaInfo_GUI_.*_Windows_x64_WithoutInstaller\.7z\)">.*/\1/p'`
  # https://mediaarea.net/download/binary/mediainfo-gui/20.08/MediaInfo_GUI_20.08_Windows_x64_WithoutInstaller.7z

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(MediaInfo_GUI_.*\.7z\)/\1/p'`
  # MediaInfo_GUI_20.08_Windows_x64_WithoutInstaller.7z

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
