#!/bin/bash

POAPPIN_NAME="ocenaudio"
POAPPIN_DESCRIPTION="Easy, fast and powerful audio editor"
POAPPIN_HOMEPAGE="https://www.ocenaudio.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.ocenaudio.com/en/download"`

  POAPPIN_URL="https://www.ocenaudio.com/downloads/ocenaudio64_portable.zip"

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*Version \([0-9].*\)<\/p>.*/\1/p' | head -n 1`
  # 3.10.13

  POAPPIN_FILENAME="ocenaudio64_portable-${POAPPIN_VERSION}.zip"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
