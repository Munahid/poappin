#!/bin/bash

POAPPIN_NAME="hijackthis"
POAPPIN_DESCRIPTION="Scans your computer for settings changed by adware, spyware, malware and other unwanted programs"
POAPPIN_HOMEPAGE="https://github.com/dragokas/hijackthis"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL="https://github.com/dragokas/hijackthis/blob/devel/LICENSE.md"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/HiJackThis-\([0-9].*\)\.exe/\1/p'`
  # 2.9.0.17
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/dragokas/hijackthis/tree/devel/binary"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a .* href="\(\/dragokas\/hijackthis\/.*\/HiJackThis\.exe\)">.*/\1/p' | sed 's/blob/raw/g'`
  # https://github.com/dragokas/hijackthis/raw/devel/binary/HiJackThis.exe

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.* href="\/dragokas\/hijackthis\/commit\/.*">\([0-9].*\)<\/a>$/\1/p' | head -n 1`
  # 2.9.0.17 - not safe, sometimes there is an error and no version information.
  # Then we load the commit url and get the version number from the title.

  if [ "${POAPPIN_VERSION}" = "" ]; then
    echo "Version was an empty string."
    POAPPIN_VERSION="https://github.com"`echo "${DATA}" | sed -n 's/.* href="\(\/dragokas\/hijackthis\/commit\/.*\)">.*<\/a>$/\1/p' | head -n 1`
    # commit url: https://github.com/dragokas/hijackthis/commit/b525bbcf0c81ce9c86f6f7becaef6cc5fc84fe56

    DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_VERSION}"`

    POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<title>\([0-9].*\) · dragokas\/hijackthis@.*$/\1/p'`
    # 2.9.0.17
  fi

  POAPPIN_FILENAME="HiJackThis-${POAPPIN_VERSION}.exe"
  # HiJackThis-2.9.0.17.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" HiJackThis.exe
}

