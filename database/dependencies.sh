#!/bin/bash

POAPPIN_NAME="dependencies"
POAPPIN_DESCRIPTION="A rewrite of the old legacy software depends.exe in C#"
POAPPIN_HOMEPAGE="https://github.com/lucasg/Dependencies"
POAPPIN_LICENSE="MIT"
POAPPIN_LICENSE_URL="https://github.com/lucasg/Dependencies/blob/master/LICENSE"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Dependencies_x64_Release-\([0-9].*\)\.zip/\1/p'`
  # 1.10
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/lucasg/Dependencies/releases/latest"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/lucasg\/Dependencies\/.*_x64_Release\.zip\)" rel=.*/\1/p'`
  # https://github.com/lucasg/Dependencies/releases/download/v1.10/Dependencies_x64_Release.zip

  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/.*download\/v\([0-9].*\)\/Depen.*/\1/p'`
  # 1.10

  POAPPIN_FILENAME="Dependencies_x64_Release-${POAPPIN_VERSION}.zip"
  # Dependencies_x64_Release-1.10.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
