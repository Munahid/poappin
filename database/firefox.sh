#!/bin/bash

POAPPIN_NAME="firefox"
POAPPIN_DESCRIPTION="Mozilla Firefox Web-Browser"
POAPPIN_HOMEPAGE="https://www.mozilla.org/de/firefox/new/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Firefox Setup \([0-9].*\)\.exe/\1/p'`
  # 77.0.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.mozilla.org/de/firefox/all/"`

  # <html class="windows x86 no-js" lang="de" dir="ltr" data-latest-firefox="77.0.1" data-esr-versions="68.9.0" data-gtm-container-id="GTM-MW3R8V"  data-stub-attribution-rate="1.0" >
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^<html .* data-latest-firefox="\(.*\)" data-esr-versions.*>$/\1/p'`
  # 77.0.1

  # <a href="https://download.mozilla.org/?product=firefox-latest-ssl&amp;os=win64&amp;lang=de"
  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a href="\(https:\/\/download\.mozilla\.org\/.*firefox-latest-ssl.*os=win64\&.*lang=de\)".*$/\1/p' | sed 's/\&amp;/\&/g'`
  # https://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=de

  # we use wget -qS to get the http headers (like curl -IL) to find out the exact filename.
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://download-installer.cdn.mozilla.net/pub/firefox/releases/77.0.1/win64/de/Firefox%20Setup%2077.0.1.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/de\/\(Firefox.*\.exe\)$/\1/p' | sed 's/%20/\ /g'`
  # Firefox Setup 77.0.1.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm setup.exe
  mv core/* .
  rm -rf core
}
