#!/bin/bash

POAPPIN_NAME="nmap"
POAPPIN_DESCRIPTION="Nmap (Network Mapper) is a utility for network discovery and security auditing"
POAPPIN_HOMEPAGE="https://nmap.org/"
POAPPIN_LICENSE="Freeware, OSS"
POAPPIN_LICENSE_URL="https://svn.nmap.org/nmap/COPYING"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# or use https://nmap.org/zenmap/ ??
# Registry tweaks: https://nmap.org/book/inst-windows.html

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/nmap-\([0-9].*\)-win32\.zip/\1/p'`
  # 7.80
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://nmap.org/download.html"`

  # <b>Latest <u>stable</u> command-line zipfile:</b> <a href="https://nmap.org/dist/nmap-7.80-win32.zip" onclick="_gaq.push(['_trackEvent', 'Downloads', 'Nmap', 'nmap-7.80-win32.zip']);">nmap-7.80-win32.zip</a>

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*Latest.*stable.*zipfile:.* <A HREF="\(https.*nmap\.org\/dist\/nmap-.*-win32\.zip\)" onClick=.*>nmap.*zip.*/\1/p'`
  # https://nmap.org/dist/nmap-7.80-win32.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*dist\/\(nmap-.*\.zip\)/\1/p'`
  # nmap-7.80-win32.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv nmap-${POAPPIN_VERSION}/* .
  rm -rf nmap-${POAPPIN_VERSION}
}

