#!/bin/bash

POAPPIN_NAME="advanced-ip-scanner"
POAPPIN_DESCRIPTION="Reliable and free network scanner to analyse LAN"
POAPPIN_HOMEPAGE="https://www.advanced-ip-scanner.com/de/"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Advanced_IP_Scanner_\([0-9].*\)\.exe/\1/p'`
  # 2.5.3850
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.advanced-ip-scanner.com/de/download/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*\/download.*Advanced_IP_Scanner.*\.exe\)" class="download_link" .*>.*/\1/p' | head -n 1`
  # https://download.advanced-ip-scanner.com/download/files/Advanced_IP_Scanner_2.5.4594.1.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download\/files\/\(Adv.*\.exe\)/\1/p'`
  # Advanced_IP_Scanner_2.5.4594.1.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #requires innoextract, lessmsi

  ${POAPPIN_APPS_DIR}/innoextract-*/innoextract.exe --extract --collisions rename "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv tmp/ip_scan_en_us_Release_*.msi .
  rm -rf tmp
  ${POAPPIN_APPS_DIR}/lessmsi-*/lessmsi x ip_scan_en_us_Release_*.msi
  rm ip_scan_en_us_Release_*.msi
  mv ip_scan_en_us_Release_*/SourceDir/Advanced\ IP\ Scanner\ v2/* .
  rm -rf ip_scan_en_us_Release_*
  #mkdir platforms
  #mkdir printsupport
  #mv qwindows.dll platforms
  #mv windowsprintersupport.dll printsupport
}
