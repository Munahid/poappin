#!/bin/bash

POAPPIN_NAME="notepad++"
POAPPIN_DESCRIPTION="Free source code editor and Notepad replacement"
POAPPIN_HOMEPAGE="https://notepad-plus-plus.org/"
POAPPIN_LICENSE="GNU GPL v2+"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^npp\.\([0-9].*[0-9]\)\.portable\.x64\.zip$/\1/p'`
  # 7.9
}

function poappin_app_check {
  # or use: https://notepad-plus-plus.org/update/getDownloadUrl.php
  # This is used by the updater, but doesn't know anything about the latest release.
  DATA=`${POAPPIN_GET_PAGE} "https://notepad-plus-plus.org/downloads/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a href="\(https.*notepad-plus-plus.org\/downloads\/v[0-9].*\/\)">.*$/\1/p' | head -n 1`
  # https://notepad-plus-plus.org/downloads/v7.9/

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/^.*li.*a href="\(https.*github\.com\/notepad-plus-plus.*\/npp\.[0-9].*\.portable\.x64\.zip\)">Portable (zip)<\/a>.*$/\1/p'`
  # https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.9/npp.7.9.portable.x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(npp\.[0-9].*\.portable.*\.zip\)$/\1/p'`
  # npp.7.9.portable.x64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -rf updater

  touch doLocalConf.xml

  cp localization/german.xml nativeLang.xml
  cp langs.model.xml langs.xml
  cp stylers.model.xml stylers.xml
}
