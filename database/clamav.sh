#!/bin/bash

POAPPIN_NAME="clamav"
POAPPIN_DESCRIPTION="Open source antivirus engine"
POAPPIN_HOMEPAGE="https://www.clamav.net/"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/clamav-\([0-9].*\)-win-x64-portable\.zip/\1/p'`
  # 0.103.0
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.clamav.net/downloads"`

  POAPPIN_URL="https://www.clamav.net"`echo "${DATA}" | sed -n 's/.*<td><a href="\(\/downloads\/production\/clamav-.*-win-x64-portable\.zip\)">clamav-.*\.zip<\/a>.*$/\1/p'`
  # https://www.clamav.net/downloads/production/clamav-0.103.0-win-x64-portable.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/production\/\(clamav-.*\.zip\)/\1/p'`
  # clamav-0.103.0-win-x64-portable.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  cp conf_examples/clamd.conf.sample clamd.conf
  cp conf_examples/freshclam.conf.sample freshclam.conf

  sed -i 's/^Example$//' freshclam.conf
  sed -i 's/^Example$//' clamd.conf

  ./freshclam.exe

  # Updating without proxy or internet connection:
  # Manually download these 3 files (about 200 MB) into the database directory:
  # http://database.clamav.net/main.cvd
  # http://database.clamav.net/daily.cvd
  # http://database.clamav.net/bytecode.cvd

  # Check with:
  #wget https://secure.eicar.org/eicar.com.txt
  #./clamscan.exe eicar.com.txt
  #...\eicar.com.txt: Win.Test.EICAR_HDB-1 FOUND
}
