#!/bin/bash

POAPPIN_NAME="coolutils-mail-viewer"
POAPPIN_DESCRIPTION="Opens MSG, EML, EMLX, ICS, VCF files"
POAPPIN_HOMEPAGE="https://www.coolutils.com/MailViewer"
POAPPIN_LICENSE="Free for government organizations, personal, educational use"
POAPPIN_LICENSE_URL="https://www.coolutils.com/MailViewerLicenses"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION="innounp"

function poappin_app_set_version {
  #if [ -f "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" ]; then
  #  POAPPIN_VERSION=`poappin_get_version_info_block "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"`
  #else
  #  echo "Download the setup file first. It contains a version number."
  #  POAPPIN_VERSION=""
  #fi

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/MailViewer-\([0-9].*\)\.exe/\1/p'`
  # 20xx-xx-xx
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.coolutils.com/MailViewer"`

  POAPPIN_URL="https://www.coolutils.com/"`echo "${DATA}" | sed -n 's/.*<a .* href="\(Downloads\/MailViewer\.exe\)" .*Download Now.*$/\1/p' | head -n 1`
  # https://www.coolutils.com/Downloads/MailViewer.exe

  POAPPIN_VERSION=`poappin_get_last_modified_date ${POAPPIN_URL}`
  # 2020-08-03

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/Downloads\/\(.*\)\.exe/\1/p'`"-${POAPPIN_VERSION}.exe"
  # MailViewer-2020-08-03.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #poappin_extract_inno_setup_file "${POAPPIN_FILENAME}"

  ../innounp-0.49/innounp.exe -x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv \{app\}/* .
  rm -rf \{app\}
}
