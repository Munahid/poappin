#!/bin/bash

POAPPIN_NAME="gpg4win"
POAPPIN_DESCRIPTION="GNU Privacy Guard for Windows"
POAPPIN_HOMEPAGE="https://www.gpg4win.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://www.gpg4win.org/
# https://www.gpg4win.de/get-gpg4win-de.html
# http://ftp.gpg4win.org/

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.gpg4win.de/thanks-for-download.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*href="\(https.*gpg4win.*\.exe\).*$/\1/p'`
  # https://files.gpg4win.org/gpg4win-3.1.11.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(gpg4win-.*\.exe\)$/\1/p'`
  # gpg4win-3.1.11.exe

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^gpg4win-\([0-9].*[0-9]\)\.exe$/\1/p'`
  # 3.1.11
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"

  # <a href="https://files.gpg4win.org/doc/gpg4win-compendium-en.pdf">Gpg4win Compendium</a>
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  rm gpg4win-uninstall.exe.nsis
  rm -rf "\$PLUGINSDIR"
  cd \$TEMP
  # gnupg-w32-2.2.19-20191207-bin.exe
  echo "Now install at least the package `ls -1 gnupg* | sed 's/-w32//' | sed 's/-20[0-9]\{6\}-bin\.exe//'` too!"
  echo "This version was inside the gpg4win archive. Please install the"
  echo "version from the website (via poappin) because this maybe a later one."
  #mv gnupg* "${POAPPIN_DOWNLOADS_DIR}/"
  cd ..
  rm -rf "\$TEMP"
}

