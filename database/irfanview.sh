#!/bin/bash

POAPPIN_NAME="irfanview"
POAPPIN_DESCRIPTION="Fast, compact and innovative graphic viewer"
POAPPIN_HOMEPAGE="https://www.irfanview.com/"
POAPPIN_LICENSE="Freeware for non-commercial use"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# We ignore the links targeting files on fosshub.com provided at the
# irfanview homepage. Also the german download mirror at heise.de.
# The download page has portable links direct from the homepage.
# The portable plugins archive is at a separate page.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/irfanview-de-\([0-9].*\)\.zip/\1/p'`
  # 4.54
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.irfanview.com/"`

  # <br><span><strong>Current version 4.54</strong></span>
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*br.*span.*strong.*Current version \([0-9].*\)<\/strong>.*/\1/p' | head -n 1`
  # 4.54

  # We need the current version number to construct a valid file name and
  # to compare with the plugins archive version number.

  DATA=`${POAPPIN_GET_PAGE} "https://www.irfanview.com/download_sites.htm"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*irfanview\.info\/files\/iview.*g\.zip\)" class=.*>Download von IrfanView.*Deutsch.*ZIP.*$/\1/p'`
  # https://www.irfanview.info/files/iview454g.zip (german version, else maybe iview454.zip)


  DATA=`${POAPPIN_GET_PAGE} "https://www.irfanview.com/plugins.htm"`

  # Install 32-bit PlugIns to IrfanView-32 and 64-bit PlugIns to IrfanView-64 folder.
  # DO NOT mix the PlugIns and IrfanView bit versions.

  # <p><strong>The current PlugIns version is: 4.54</strong>&nbsp;</p>
  POAPPIN_PLUGINS_VERSION=`echo "${DATA}" | sed -n 's/.*<strong>The current PlugIns version is: \([0-9].*\)<\/strong>.*$/\1/p'`
  # 4.54

  if ! [[ "${POAPPIN_VERSION}" = "${POAPPIN_PLUGINS_VERSION}" ]]; then
    echo "Warning: Plugins version and application version do not match."
    echo "Package version: ${POAPPIN_VERSION}"
    echo "Plugins version: ${POAPPIN_PLUGINS_VERSION}"
  fi

  unset POAPPIN_PLUGINS_VERSION

  POAPPIN_PLUGINS_URL=`echo "${DATA}" | sed -n 's/.*<br \/> Alternative download site:<a href="\(https.*irfanview\.info\/files\/iview.*a_plugins\.zip\)">iview.*plugins.*zip<\/a>.*$/\1/p'`
  # https://www.irfanview.info/files/iview454a_plugins.zip

  POAPPIN_PLUGINS_FILENAME="${POAPPIN_NAME}-plugins-${POAPPIN_VERSION}.zip"
  # irfanview-plugins-4.54.zip

  POAPPIN_FILENAME="${POAPPIN_NAME}-de-${POAPPIN_VERSION}.zip"
  # irfanview-de-4.54.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" --referer="${POAPPIN_URL}" "${POAPPIN_URL}"

  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_PLUGINS_FILENAME}" --referer="${POAPPIN_PLUGINS_URL}" "${POAPPIN_PLUGINS_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  rm -f iv_uninstall.exe
}

