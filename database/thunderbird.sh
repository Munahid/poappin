#!/bin/bash

POAPPIN_NAME="thunderbird"
POAPPIN_DESCRIPTION="Mozilla Thunderbird E-Mail application"
POAPPIN_HOMEPAGE="https://www.thunderbird.net/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  POAPPIN_URL="https://www.thunderbird.net/en-US/thunderbird/all/"

  # or hard code this URL: https://download.mozilla.org/?product=thunderbird-latest&os=win&lang=de

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # <a href="https://download.mozilla.org/?product=thunderbird-68.9.0-SSL&os=win64&lang=de" title="Download for Windows 64-bit in German"
  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a href="\(https.*download\.mozilla\.org.*product=thunderbird.*win64.*lang=de\)" title=.*$/\1/p' | grep -v msi`
  # https://download.mozilla.org/?product=thunderbird-68.9.0-SSL&os=win64&lang=de
 
  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/https.*thunderbird-\([0-9].*[0-9]\)-SSL.*$/\1/p'`
  # 68.9.0

  # we use wget -qS to get the http headers (like curl -IL) to find out the exact filename.
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://download-installer.cdn.mozilla.net/pub/thunderbird/releases/68.9.0/win64/de/Thunderbird%20Setup%2068.9.0.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/de\/\(Thunderbird.*\.exe\)$/\1/p' | sed 's/%20/\ /g'`
  # Thunderbird Setup 68.9.0.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  rm setup.exe
  mv core/* .
  rm -rf core
  # Added after comparisation with an real installation.
  cp MapiProxy.dll MapiProxy_InUse.dll
  cp mozMapi32.dll mozMapi32_InUse.dll
}
