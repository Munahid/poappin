#!/bin/bash

POAPPIN_NAME="cherry-tree"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="https://www.giuspen.com/cherrytree/"
POAPPIN_LICENSE="GNU GPL v3+"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/cherrytree_\([0-9].*\)_win64_portable\.7z/\1/p'`
  # 0.99.22.0
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.giuspen.com/cherrytree/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https:\/\/www.giuspen.com\/.*portable\.7z\)" rel=.*>cherrytree.*\.7z<\/a>.*/\1/p'`
  # https://www.giuspen.com/software/cherrytree_0.99.22.0_win64_portable.7z

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*software\/\(cherrytree_.*\.7z\)/\1/p'`
  # cherrytree_0.99.22.0_win64_portable.7z

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv cherrytree_*_win64_portable/* .
  rm -rf cherrytree_*_win64_portable

  # look for mingw64/bin/cherrytree.exe
  # for portable config put config.cfg in
  # portable folder root, beside license.txt.

  touch config.cfg

}
