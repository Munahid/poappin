#!/bin/bash

POAPPIN_NAME="foobar2000"
POAPPIN_DESCRIPTION="Advanced freeware audio player for Windows"
POAPPIN_HOMEPAGE="https://www.foobar2000.org/"
POAPPIN_LICENSE="proprietary"
POAPPIN_LICENSE_URL="https://www.foobar2000.org/license"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/foobar2000_v\([0-9].*[0-9]\)\.exe/\1/p'`
  # 1.5.5
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.foobar2000.org/download"`

  POAPPIN_URL="https://www.foobar2000.org"`echo "${DATA}" | sed -n 's/Download <a href="\(\/getfile\/foobar2000_v.*\.exe\)">foobar.*$/\1/p' | head -n 1 | sed 's/getfile/files/'`
  # https://www.foobar2000.org/files/7dc49f51c16126965996959907a13d1d/foobar2000_v1.6.2.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*files.*\/\(foobar2000.*\.exe\)$/\1/p'`
  # foobar2000_v1.6.2.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  # extract with auto renaming files.
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm uninstall.exe
  rm -rf \$PLUGINSDIR
  mv \$R0 ShellExt32.dll
  mv \$R0_1 ShellExt64.dll
  touch user_profiles_enabled
}

function poappin_app_uninstall {
  reg delete "HKEY_CURRENT_USER\Software\foobar2000" //f
}
