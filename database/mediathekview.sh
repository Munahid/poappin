#!/bin/bash

POAPPIN_NAME="mediathekview"
POAPPIN_DESCRIPTION="Searches the online video archives of some german TV channels (ARD, ZDF, Arte, 3Sat, SWR, BR, MDR, NDR, WDR, HR, RBB, ORF, SRF)"
POAPPIN_HOMEPAGE="https://mediathekview.de/"
POAPPIN_LICENSE="GNU GPL v3"
POAPPIN_LICENSE_URL="https://mediathekview.de/lizenz/"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/MediathekView-\([0-9].*\)\.zip/\1/p'`
  # 13.5.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://mediathekview.de/download/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*mediathekview\.de.*\/MediathekView-.*-win\.zip\)">.*/\1/p'`
  # https://download.mediathekview.de/stabil/MediathekView-latest-win.zip

  DATA=`${POAPPIN_GET_PAGE} "https://download.mediathekview.de/stabil/"`

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/<a href="'"$(basename ${POAPPIN_URL})"'">.*\([0-9][0-9]-...-20[0-9][0-9]\) .*/\1/p'`
  # 19-Jan-2020
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/<a href="MediathekView-\([0-9].*\)-win\.zip">.*'${POAPPIN_VERSION}' .*$/\1/p'`
  # 13.5.1

  POAPPIN_FILENAME="MediathekView-${POAPPIN_VERSION}.zip"
  # MediathekView-13.5.1.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv MediathekView/* .
  mv MediathekView/.install4j .
  rm -rf MediathekView
}

