#!/bin/bash

POAPPIN_NAME="ghidra"
POAPPIN_DESCRIPTION="A software reverse engineering (SRE) suite of tools developed by the NSA"
POAPPIN_HOMEPAGE="https://ghidra-sre.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/ghidra_\([0-9].*\)_PUBLIC_20......\.zip/\1/p'`
  # 9.1.2
}

function poappin_app_check {
  # Source here: https://github.com/NationalSecurityAgency/ghidra
  DATA=`${POAPPIN_GET_PAGE} "https://ghidra-sre.org/"`

  POAPPIN_URL="https://ghidra-sre.org/"`echo "${DATA}" | sed -n 's/.*<a class=.* href="\(ghidra_.*_PUBLIC_.*\.zip\)" .*Download Ghidra.*$/\1/p'`
  # https://ghidra-sre.org/ghidra_9.1.2_PUBLIC_20200212.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(ghidra_.*\.zip\)/\1/p'`
  # ghidra_9.1.2_PUBLIC_20200212.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv ghidra*/* .
  rm -rf ghidra*PUBLIC
  sync
}

