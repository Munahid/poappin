#!/bin/bash

POAPPIN_NAME="scribus"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="https://www.scribus.net/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/scribus-\([0-9].*\)-windows-x64\.exe/\1/p'`
  # 1.4.8
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.scribus.net/downloads/stable-branch/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/<a href="\(http.*sourceforge.*\/files\/scribus\/.*\/\)" target=.*>.*/\1/p' | head -n 1`
  # https://sourceforge.net/projects/scribus/files/scribus/1.4.8/

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*files\/scribus\/.*\/scribus-.*-windows-x64\.exe\/download\)"/\1/p'`
  # https://sourceforge.net/projects/scribus/files/scribus/1.4.8/scribus-1.4.8-windows-x64.exe/download

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(scribus-.*-windows-x64\.exe\)\/download/\1/p'`
  # scribus-1.4.8-windows-x64.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  # \$TEMP - contains vsredist files:
  # Microsoft Visual C++ 2008 Redistributable Setup
  # Microsoft Visual C++ 2012 Redistributable (x64) - 11.0.61030
  rm -rf \$PLUGINSDIR uninst.exe
}
