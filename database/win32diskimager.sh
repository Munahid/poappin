#!/bin/bash

POAPPIN_NAME="win32diskimager"
POAPPIN_DESCRIPTION="Disk Imager"
POAPPIN_HOMEPAGE="https://sourceforge.net/projects/win32diskimager/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  POAPPIN_URL="https://sourceforge.net/projects/win32diskimager/files/Archive/"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # <a class="..." href="/projects/win32diskimager/files/latest/download" title="/Archive/win32diskimager-1.0.0-install.exe:  released on 2017-03-08 01:56:10 UTC">
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/^.*<a class=".*" href=".*files\/latest\/download" title=".*\/win32diskimager-\([0-9].*\)-install\.exe.*$/\1/p'`
  # 1.0.0

  # <a href="https://sourceforge.net/projects/win32diskimager/files/Archive/Win32DiskImager-1.0.0-binary.zip/download" title="Click to download Win32DiskImager-1.0.0-binary.zip">
  POAPPIN_URL=`echo "${DATA}" | sed -n 's/^.*<a href="\(https.*win32diskimager.*binary\.zip\/download\)".*$/\1/p' | grep $POAPPIN_VERSION`
  # https://sourceforge.net/projects/win32diskimager/files/Archive/Win32DiskImager-1.0.0-binary.zip/download

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
