#!/bin/bash

POAPPIN_NAME="autopsy"
POAPPIN_DESCRIPTION="Digital forensics platform and graphical interface to The Sleuth Kit"
POAPPIN_HOMEPAGE="https://www.sleuthkit.org/autopsy/"
POAPPIN_LICENSE="misc"
POAPPIN_LICENSE_URL="https://www.sleuthkit.org/autopsy/licenses.php"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/autopsy-\([0-9].*[0-9]\)\.zip$/\1/p'`
  # 4.15.0
}

function poappin_app_check {
  # https://github.com/sleuthkit/autopsy/releases/
  DATA=`${POAPPIN_GET_PAGE} "https://www.autopsy.com/download/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*Download.*<a href="\(https.*github.*sleuthkit\/autopsy.*\.zip\)">.*$/\1/p'`
  # https://github.com/sleuthkit/autopsy/releases/download/autopsy-4.15.0/autopsy-4.15.0.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download\/autopsy.*\/\(autopsy.*\.zip\)$/\1/p'`
  # autopsy-4.15.0.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv autopsy-*/* .
  sync
  rm -rf autopsy-*

  # https://github.com/sleuthkit/autopsy_addon_modules
}
