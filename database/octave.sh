#!/bin/bash

POAPPIN_NAME="octave"
POAPPIN_DESCRIPTION="Scientific Programming Language"
POAPPIN_HOMEPAGE="https://www.gnu.org/software/octave/"
POAPPIN_LICENSE="GNU GPL"
POAPPIN_LICENSE_URL="https://www.gnu.org/software/octave/license.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/octave-\([0-9].*\)-w64\.7z/\1/p' | sed 's/_/\./g'`
  # 5.2.0.1
}

function poappin_app_check {
  # https://ftpmirror.gnu.org/octave/windows/
  # https://ftp.gnu.org/gnu/octave/windows/
  DATA=`${POAPPIN_GET_PAGE} "https://www.gnu.org/software/octave/download.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*gnu\.org\/octave\/windows\/octave-.*-w64\.7z\)">octave.*/\1/p'`
  # https://ftpmirror.gnu.org/octave/windows/octave-5.2.0_1-w64.7z

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*windows\/\(octave-.*-w64\.7z\)/\1/p'`
  # octave-5.2.0_1-w64.7z

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv octave-*-w64/* .
  rm -rf octave-*-w64
}
