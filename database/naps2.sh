#!/bin/bash

POAPPIN_NAME="naps2" # Not Another PDF Scanner 2
POAPPIN_DESCRIPTION="Scan documents to PDF and other file types"
POAPPIN_HOMEPAGE="https://www.naps2.com/"
POAPPIN_LICENSE="GNU GPL v2+"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/naps2-\([0-9].*[0-9]\)-portable\.zip/\1/p'`
  # 6.1.2
}

function poappin_app_check {
  # In case of problems, look here too:
  # https://sourceforge.net/projects/naps2/
  # https://sourceforge.net/projects/naps2/files/latest/download
  # https://netcologne.dl.sourceforge.net/project/naps2/6.1.2/naps2-6.1.2-portable.zip

  DATA=`${POAPPIN_GET_PAGE} "https://www.naps2.com/download.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github\.com\/cyanfish\/naps2\/.*\/naps2-.*-portable\.zip\)">.*$/\1/p'`
  # https://github.com/cyanfish/naps2/releases/download/v6.1.2/naps2-6.1.2-portable.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(naps2-.*-portable\.zip\)/\1/p'`
  # naps2-6.1.2-portable.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
