#!/bin/bash

POAPPIN_NAME="blat"
POAPPIN_DESCRIPTION="A Windows command line SMTP mailer"
POAPPIN_HOMEPAGE="https://www.blat.net/"
POAPPIN_LICENSE="Public Domain"
POAPPIN_LICENSE_URL="https://www.blat.net/?docs/license.txt"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/blat\([0-9].*\)_64\.full\.zip/\1/p' | sed 's/\([0-9]\)\([0-9]\)\([0-9]\{2\}\)/\1.\2.\3/'`
  # 3.2.22
}

function poappin_app_check {
  # only 32 bit version here: https://sourceforge.net/projects/blat/files/latest/download
  POAPPIN_URL="https://sourceforge.net/projects/blat/files/Blat%20Full%20Version/64%20bit%20versions/"

  DATA=`${POAPPIN_GET_PAGE} ${POAPPIN_URL}`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*href="\(http.*\.zip\/download\)".*$/\1/p' | head -n 1`
  # https://sourceforge.net/projects/blat/files/Blat%20Full%20Version/64%20bit%20versions/blat3222_64.full.zip/download

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*versions\/\(blat.*\.zip\)\/download$/\1/p'`
  # blat3222_64.full.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv blat*/* .
  rm -rf blat*
  mv full/* . && rm -rf full
}
