#!/bin/bash

POAPPIN_NAME="cmdow"
POAPPIN_DESCRIPTION="Win32 console application for manipulating program windows"
POAPPIN_HOMEPAGE="https://ritchielawrence.github.io/cmdow/"
POAPPIN_LICENSE="MIT License"
POAPPIN_LICENSE_URL="https://github.com/ritchielawrence/cmdow/blob/master/LICENSE.txt"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/cmdow-\([0-9].*\)\.exe/\1/p'`
  # 1.4.8
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://raw.githubusercontent.com/ritchielawrence/cmdow/master/VERSION.txt"`
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/This version of Cmdow is v\([0-9].*\)$/\1/p'`
  # 1.4.8

  #DATA=`${POAPPIN_GET_PAGE} "https://github.com/ritchielawrence/cmdow/tree/master/bin/Release"`
  POAPPIN_URL="https://github.com/ritchielawrence/cmdow/raw/master/bin/Release/cmdow.exe"

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.exe"
  # cmdow-1.4.8.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "cmdow.exe"
}
