#!/bin/bash

POAPPIN_NAME="monero-cli"
POAPPIN_DESCRIPTION="Monero CLI Wallet"
POAPPIN_HOMEPAGE="https://www.getmonero.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/monero-win-x64-v\([0-9].*\)\.zip/\1/p'`
  # 0.17.2.3
}

function poappin_app_check {
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} "https://downloads.getmonero.org/cli/win64" 2>&1 | grep Location | sed -n 's/.*Location: \(https.*zip\)$/\1/p'`
  # https://downloads.getmonero.org/cli/monero-win-x64-v0.17.2.3.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*cli\/\(monero-win-x64-.*\.zip\)/\1/p'`
  # monero-win-x64-v0.17.2.3.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
  sha256sum "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  # Compare with website: https://www.getmonero.org/downloads/
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv mon* xxx
  mv xxx/* .
  sync
  rm -rf xxx
  sync
}
