#!/bin/bash

POAPPIN_NAME="crystaldiskinfo"
POAPPIN_DESCRIPTION="A HDD/SSD utility software which supports a part of USB, Intel/AMD RAID and NVMe"
POAPPIN_HOMEPAGE="https://crystalmark.info/en/software/crystaldiskinfo/"
POAPPIN_LICENSE="MIT"
POAPPIN_LICENSE_URL="https://crystalmark.info/en/software/crystaldiskinfo/crystaldiskinfo-license/"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/CrystalDiskInfo\([0-9].*\)\.zip/\1/p' | sed 's/_/./g'`
  # 8.8.9
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://crystalmark.info/en/download/#CrystalDiskInfo"`

  POAPPIN_URL="https://crystalmark.info"`echo "${DATA}" | sed -n 's/.*<a class=.* href="\(\/redirect.*product=CrystalDiskInfo\)">.*ZIP.*/\1/p'`
  # https://crystalmark.info/redirect.php?product=CrystalDiskInfo
  # <link rel="canonical" href="https://osdn.net/projects/crystaldiskinfo/downloads/73507/CrystalDiskInfo8_8_9.zip/">
  # <link rel="alternate" hreflang="de" href="https://de.osdn.net/projects/crystaldiskinfo/downloads/73507/CrystalDiskInfo8_8_9.zip/" />
  # This URL would work as download source due to some redirects but we want the exact version number too.

  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p' | head -n 1`
  # https://osdn.net/dl/crystaldiskinfo/CrystalDiskInfo8_8_9.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*crystaldiskinfo\/\(CrystalDiskInfo.*\.zip\)/\1/p'`
  # CrystalDiskInfo8_8_9.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
