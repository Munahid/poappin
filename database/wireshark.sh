#!/bin/bash

POAPPIN_NAME="wireshark"
POAPPIN_DESCRIPTION="Network protocol analyzer"
POAPPIN_HOMEPAGE="https://www.wireshark.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Wireshark-win64-\([0-9].*[0-9]\)\.exe$/\1/p'`
  # 3.2.5
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.wireshark.org/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a href="\(https.*wireshark\.org\/win64\/Wireshark-win64-.*\.exe\)">Windows Installer.*$/\1/p' | head -n 1`
  # There are two URLs: The 2nd one is the previous (old) version.
  # https://1.eu.dl.wireshark.org/win64/Wireshark-win64-3.2.5.exe
  # https://1.eu.dl.wireshark.org/win32/WiresharkPortable_3.2.5.paf.exe better?

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*win64\/\(Wireshark-win64-.*\.exe\)/\1/p'`
  # Wireshark-win64-3.2.5.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  # vcredist_x64.exe /install /passive or /quiet instead of /passive (stands for no GUI) - invokes UAC.

  # TODO: Needs wix installed...
  ../wix-3.11.2/dark.exe -x jjjj vcredist_x64.exe
  "${POAPPIN_PATH_TO_7ZIP}" x jjjj/AttachedContainer/packages/vcRuntimeMinimum_amd64/cab1.cab
  rm -rf jjjj
  sync
}
