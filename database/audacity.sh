#!/bin/bash

POAPPIN_NAME="audacity"
POAPPIN_DESCRIPTION="Free, open source, cross-platform audio software"
POAPPIN_HOMEPAGE="https://www.audacityteam.org/"
POAPPIN_LICENSE="GNU GPL 2+"
POAPPIN_LICENSE_URL="https://www.audacityteam.org/about/license/"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://github.com/audacity/audacity/releases

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/audacity-win-\([0-9].*\)-64bit\.exe/\1/p'`
  # 3.1.2
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.audacityteam.org/download/windows/"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github\.com.*\/audacity-win-[0-9].*-64bit\.exe\)">direct link<\/a>.*$/\1/p'`
  # https://github.com/audacity/audacity/releases/download/Audacity-3.1.2/audacity-win-3.1.2-64bit.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(audacity-win.*\.exe\)/\1/p'`
  # audacity-win-3.1.2-64bit.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #requires innoextract

  ${POAPPIN_APPS_DIR}/innoextract-1.9/innoextract.exe -e --collisions rename "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  sync
  mv app/* .
  sync
  rm -rf app
  sync
}
