#!/bin/bash

POAPPIN_NAME="libre-office"
POAPPIN_DESCRIPTION="Free and powerful office suite"
POAPPIN_HOMEPAGE="https://de.libreoffice.org/"
POAPPIN_LICENSE="Mozilla Public License Version 2.0"
POAPPIN_LICENSE_URL="https://www.mozilla.org/en-US/MPL/2.0/"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # see also: https://de.libreoffice.org/download/portable-versions/

  DATA=`${POAPPIN_GET_PAGE} "https://de.libreoffice.org/download/download/?type=win-x86_64&lang=de"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a class=.* href="\(https.*LibreOffice.*Win_x64\.msi\)">.*$/\1/p' | head -n 1`
  # https://de.libreoffice.org/donate/dl/win-x86_64/6.4.5/de/LibreOffice_6.4.5_Win_x64.msi

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(LibreOffice_.*\.msi\)/\1/p'`
  # LibreOffice_6.4.5_Win_x64.msi

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/LibreOffice_\([0-9].*[0-9]\)_Win_x64\.msi/\1/p'`
  # 6.4.5
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_msi_file "${POAPPIN_FILENAME}"

  rm LibreOffice*.msi
  sync
}
