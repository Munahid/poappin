#!/bin/bash

POAPPIN_NAME="eclipse-adoptium-jdk"
POAPPIN_DESCRIPTION="Eclipse Adoptium Java Development Kit (JDK)"
POAPPIN_HOMEPAGE="https://projects.eclipse.org/projects/adoptium"
POAPPIN_LICENSE="GPLv2 with Classpath Exception and OpenJDK Assembly Exception (often referred to as GPL2+CE)"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# We don't use the standard download web page, because it uses
# JavaScript to render the matching URLs according to the selection:
# https://adoptopenjdk.net/archive.html?variant=openjdk11&jvmVariant=hotspot
# They provide an download API: https://api.adoptopenjdk.net/
#
# https://github.com/AdoptOpenJDK/openjdk-api-v3/blob/master/docs/STRUCTURE.md
# https://api.adoptopenjdk.net/swagger-ui/
# https://api.adoptopenjdk.net/v3/binary/latest/11/ea/windows/x64/jre/hotspot/normal/adoptopenjdk

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/OpenJDK11U-jdk_x64_windows_hotspot_\([0-9].*[0-9]\)\.zip$/\1/p'`
  # 11.0.8_10
}

function poappin_app_check {
  # Version: 8 (LTS), 9, 10, 11 (LTS), 12, 13, 14, 15 (latest)
  # https://api.adoptopenjdk.net/v3/info/available_releases
  ECLIPSE_ADOPTIUM_JDK_VERSION=11

  # JVM Variant: HotSpot (OpenJDK community, used in Oracles's JDK) - "hotspot",
  # Eclipse OpenJ9 (Eclipse community, used in IBM's JDK) - "openj9".
  ECLIPSE_ADOPTIUM_JDK_JVM_VARIANT=hotspot

  # General Availability (ga) or Early Access (ea) builds?
  ECLIPSE_ADOPTIUM_JDK_RELEASE_VERSION="ga"

  POAPPIN_URL="https://api.adoptopenjdk.net/v3/assets/feature_releases/${ECLIPSE_ADOPTIUM_JDK_VERSION}/${ECLIPSE_ADOPTIUM_JDK_RELEASE_VERSION}?architecture=x64&heap_size=normal&image_type=jdk&jvm_impl=${ECLIPSE_ADOPTIUM_JDK_JVM_VARIANT}&os=windows&page=0&page_size=10&project=jdk&sort_method=DATE&sort_order=DESC&vendor=adoptopenjdk"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | grep \"link | grep \.zip | head -n 1 | sed -n 's/.*\(https.*zip\).*/\1/p' | sed 's/%2B/+/g'`
  # https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8+10/OpenJDK11U-jdk_x64_windows_hotspot_11.0.8_10.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(OpenJDK11U-.*\.zip\)$/\1/p'`
  # OpenJDK11U-jdk_x64_windows_hotspot_11.0.8_10.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv jdk-*/* .
  rm -rf jdk-*
}
