#!/bin/bash

POAPPIN_NAME="supertuxkart"
POAPPIN_DESCRIPTION="SuperTuxKart is a 3D open-source arcade racer with a variety characters, tracks, and modes to play."
POAPPIN_HOMEPAGE="https://supertuxkart.net/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/SuperTuxKart-\([0-9].*\)-win\.zip/\1/p'`
  # 1.2
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/supertuxkart/stk-code/releases/latest"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/supertuxkart.*\/SuperTuxKart-.*-win.zip\)" rel=.*>/\1/p'`
  # https://github.com/supertuxkart/stk-code/releases/download/1.2/SuperTuxKart-1.2-win.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(SuperTuxKart-.*\.zip\)/\1/p'`
  # SuperTuxKart-1.2-win.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
