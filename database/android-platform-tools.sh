#!/bin/bash

POAPPIN_NAME="android-platform-tools"
POAPPIN_DESCRIPTION="Android SDK part providing adb, fastboot, and systrace"
POAPPIN_HOMEPAGE="https://developer.android.com/studio/releases/platform-tools"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# You need this package for updating your Android smartphone using adb.
# https://wiki.lineageos.org/adb_fastboot_guide.html

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/platform-tools_r\([0-9].*\)-windows\.zip/\1/p'`
  # 30.0.4
}

function poappin_app_check {
  POAPPIN_URL="https://dl.google.com/android/repository/platform-tools-latest-windows.zip"
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://dl.google.com/android/repository/platform-tools_r30.0.4-windows.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/repository\/\(platform-tools.*\.zip\)/\1/p'`
  # platform-tools_r30.0.4-windows.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv platform-tools/* .
  rm -rf platform-tools
}

