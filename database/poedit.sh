#!/bin/bash

POAPPIN_NAME="poedit"
POAPPIN_DESCRIPTION="Powerful and intuitive translation editor"
POAPPIN_HOMEPAGE="https://poedit.net/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Poedit-\([0-9].*\)-setup\.exe/\1/p'`
  # 2.4.2
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://poedit.net/download"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*a href="\(https.*\/Poedit-.*\.exe\)" target=.*/\1/p' | grep -v Poedit-1 | head -n 1`
  # https://download.poedit.net/Poedit-2.4.2-setup.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\.net\/\(Poedit-.*\.exe\)/\1/p'`
  # Poedit-2.4.2-setup.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #poappin_extract_inno_setup_file "${POAPPIN_FILENAME}"

  ${POAPPIN_APPS_DIR}/innoextract-1.9/innoextract.exe -e --collisions rename "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  mv app/* .
  rm -rf app
}
