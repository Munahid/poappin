#!/bin/bash

POAPPIN_NAME="diskinternals-linux-reader"
POAPPIN_DESCRIPTION="Access files and folders on Ext, UFS, HFS, ReiserFS, or APFS file systems from Windows"
POAPPIN_HOMEPAGE="https://www.diskinternals.com/linux-reader/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Linux_Reader-\([0-9].*\)\.exe/\1/p'`
  # 4.6
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.diskinternals.com/linux-reader/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<A .* href="\(https.*diskinternals\.com.*Linux_Reader\.exe\)" .*>GET IT FREE.*/\1/p'`
  # https://eu.diskinternals.com/download/Linux_Reader.exe
  # https://eu.diskinternals.com/download/Linux_Reader.exe?qr=1600165044251

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*GET IT FREE<span>Ver \([0-9].*\)\, Win<\/span>.*/\1/p'`
  # 4.6

  POAPPIN_FILENAME="Linux_Reader-${POAPPIN_VERSION}.exe"
  # Linux_Reader-4.6.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  rm -rf \$PLUGINSDIR
}
