#!/bin/bash

POAPPIN_NAME="target-3001-discover"
POAPPIN_DESCRIPTION="PCB Layout CAD"
POAPPIN_HOMEPAGE="https://server.ibfriedrich.com/wiki/ibfwikide/index.php?title=Hauptseite"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/target3001_discover-\([0-9].*\)\.exe/\1/p'`
  # 30.1.0.38
}

# <small>(Version 30.1.0.38, 2020-08-06) Freeware-Version:<br>250 Pins

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://server.ibfriedrich.com/wiki/ibfwikide/index.php?title=Download"`

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*(Version \([0-9].*\)\, 20..-..-..) .*/\1/p'`
  # 30.1.0.38

  DATA=`${POAPPIN_GET_PAGE} "https://ibfriedrich.com/de/index.html?showModal=yes"`
  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*window.open..\(https.*\.exe\)..;$/\1/p' | uniq`
  # https://ibfriedrich.com/target/discover/target3001_discover.exe

  POAPPIN_FILENAME="target3001_discover-${POAPPIN_VERSION}.exe"
  # target3001_discover-30.1.0.38.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  rm -rf \$PLUGINSDIR
  rm TarV30_2.exe TarV30.exe  # EN and FR version
  mv TarV30_1.exe TarV30.exe  # keep DE version
}

