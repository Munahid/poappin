#!/bin/bash

POAPPIN_NAME="gnupg"
POAPPIN_DESCRIPTION="GNU Privacy Guard (Core Files)"
POAPPIN_HOMEPAGE="https://gnupg.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# gpg4win contains a gnupg archive.
#echo "Now install at least the package `ls -1 gnupg* | sed 's/-w32//' | sed 's/-20[0-9]\{6\}-bin\.exe//'` too!"
#echo "This version was inside the gpg4win archive. Please install the"
#echo "version from the website (via poappin) because this is maybe a later one."

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/^gnupg-w32-\([0-9].*[0-9]\)_20.*\.exe$/\1/p'`
  # 2.2.20
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://gnupg.org/download/"`

  POAPPIN_URL="https://gnupg.org/"`echo "${DATA}" | sed -n 's/.*href="\(\/ftp.*\.exe\).*$/\1/p' | grep -v w32cli`
  # https://gnupg.org/ftp/gcrypt/binary/gnupg-w32-2.2.20_20200320.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\(gnupg-w32-.*\.exe\)$/\1/p'`
  # gnupg-w32-2.2.20_20200320.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm gnupg-uninstall.exe.nsis
  rm -rf "\$PLUGINSDIR"
}

