#!/bin/bash

POAPPIN_NAME="raspberry-pi-imager"
POAPPIN_DESCRIPTION="Install Raspberry Pi OS using Raspberry Pi Imager"
POAPPIN_HOMEPAGE="https://www.raspberrypi.org/software/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/imager_\([0-9].*\)\.exe/\1/p'`
  # 1.7.3
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.raspberrypi.org/software/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*downloads\.raspberrypi\.org.*\/imager_.*\.exe\)" .*Windows download.*$/\1/p'`
  # https://downloads.raspberrypi.org/imager/imager_latest.exe

  POAPPIN_URL=`wget --max-redirect 0 --spider --server-response "${POAPPIN_URL}" 2>&1 | sed -n 's/^  Location: \(https.*\)$/\1/p'`
  # https://downloads.raspberrypi.org/imager/imager_1.7.3.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/imager\/\(imager_.*\.exe\)/\1/p'`
  # imager_1.7.3.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  rm -rf \$PLUGINSDIR uninstall.exe
}
