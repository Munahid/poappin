#!/bin/bash

POAPPIN_NAME="carotdav"
POAPPIN_DESCRIPTION="Simple WebDAV / FTP / SFTP / Online Storages client for Windows"
POAPPIN_HOMEPAGE="http://rei.to/carotdav_en.html"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL="http://rei.to/carotdav_en.html#license"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/CarotDAV-portable-\([0-9].*\)\.zip/\1/p'`
  # 1.15.9
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "http://rei.to/carotdav_en.html#download"`

  POAPPIN_URL="http://rei.to/"`echo "${DATA}" | sed -n 's/.*<A href="\(CarotDAV.*portable\.zip\)">Ver. .*/\1/p' | head -n 1`
  # http://rei.to/CarotDAV1.15.9.portable.zip (the other zip contains a msi archive)

  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/.*\/CarotDAV\([0-9].*\)\.portable\.zip/\1/p'`
  # 1.15.9

  POAPPIN_FILENAME="CarotDAV-portable-${POAPPIN_VERSION}.zip"
  # CarotDAV-portable-1.15.9.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv CarotDAV/* .
  rm -rf CarotDAV
}
