#!/bin/bash

POAPPIN_NAME="softperfect-network-scanner"
POAPPIN_DESCRIPTION="Fast, highly configurable IPv4/IPv6 scanner"
POAPPIN_HOMEPAGE="https://www.softperfect.com/products/networkscanner/"
POAPPIN_LICENSE="Freeware (old version only)"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/netscan_setup-\([0-9].*\)\.exe/\1/p'`
  # 6.2.1
}

function poappin_app_check {
  # The latest freeware version is 6.2.1

  DATA=`${POAPPIN_GET_PAGE} "https://filehippo.com/download_softperfect-network-scanner/6.2.1/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a title="Download" href="\(https.*filehippo.*\)" class="program-button.*/\1/p'`
  # https://filehippo.com/download_softperfect-network-scanner/6.2.1/post_download/

  POAPPIN_VERSION="6.2.1"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.* data-qa-download-url="\(https.*filehippo.*netscan.*\.exe.*\.exe\)">/\1/p' | sed 's/&amp;/\&/g'`     
  # https://dl5.filehippo.com/8af/37d/8fbfa08c62c0fa03345c23a4f7fa57c1f7/netscan_setup.exe?Expires=1601801567&Signature=cd3fdacccca6a9d5ae7d462a7e2b1eb4bdc53320&url=https://filehippo.com/download_softperfect-network-scanner/&Filename=netscan_setup.exe

  POAPPIN_FILENAME="netscan_setup-${POAPPIN_VERSION}.exe"
  # netscan_setup-6.2.1.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #requires innoextract

  ${POAPPIN_APPS_DIR}/innoextract-1.9/innoextract.exe -e --collisions rename "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv app/netscan.exe\$0 netscan.exe   # 64 bit version
  rm -rf app
}
