#!/bin/bash

POAPPIN_NAME="opera"
POAPPIN_DESCRIPTION="Opera is a secure, innovative browser with a built-in ad blocker, free VPN, units converter, social messengers, battery saver and much more"
POAPPIN_HOMEPAGE="https://www.opera.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Opera_\([0-9].*\)_Setup_x64\.exe/\1/p'`
  # 80.0.4170.16
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.opera.com/de/computer/thanks?partner=www&par=id=55040%26location=415&gaprod=opera"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*download\.opera\.com.*get.*\)" id=.*bitte erneut.*/\1/p' | sed 's/\&amp;/\&/g'`
  # https://download.opera.com/download/get/?id=55040&location=415&nothanks=yes&sub=marine&utm_tryagain=yes

  # We look into the http headers to find out the exact filename.
  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://download3.operacdn.com/pub/opera/desktop/80.0.4170.16/win/Opera_80.0.4170.16_Setup_x64.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download.*desktop.*win\/\(Opera_.*\.exe\)/\1/p'`
  # Opera_80.0.4170.16_Setup_x64.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
