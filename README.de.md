# [Portable Applications Installer (poappin)](https://gitlab.com/Munahid/poappin/)

_Das bash-Skript "poappin" ist ein kleines Kommandozeilentool, welches es erlaubt, Anwendungen aus dem Netz zu installieren und automatisch zu aktualisieren ohne deren Setup oder `msiexec /i` aufzurufen._


# Sinn und Zweck

Kennen Sie das? Sie haben ein kleines Programm im Internet oder auf einer Zeitschrift gefunden, sie finden es praktisch und kopieren es irgendwo ins Dateisystem. Da wird es dann vergessen, bis es irgendwann mal wieder benötigt wird. Nun wird es tatsächlich gebraucht und es läuft nicht mehr: Weil zwischenzeitlich sich Verzeichnisnamen oder Laufwerksbuchstaben geändert haben, der Rechner aktualisiert wurde, die Konfiguration verändert wurde, ein Kollege es bewusst entfernt hatte, weil es für ihn nicht wichtig genug war oder auch nur, weil ein Virenschutz-Programm die eine oder andere Datei nicht mochte. Auf Software, die nicht gepflegt und genutzt wird, ist kein Verlass.

Das Skript sorgt dafür, dass ihre Software immer neu beschafft und in gleicher Art und Weise installiert wird. Dabei werden immer die neuesten Versionen installiert. Dazu muss keine herstellerspezifische Aktualisierungssoftware ständig im Hintergrund gestartet werden. Installationen mit Administrator-Rechten sind unnötig. Das Skript sorgt dafür, dass sie ein Verzeichnis haben, in welchem die von ihnen benötigte Software jederzeit in aktueller Form vorliegt. Sie müssen die Anwendung nur noch starten.

Angefangen von kleinen Tools, die als einzelne, ausführbare Binärdatei kommen, bis hin zu großen Anwendungen, die ohne Installation einfach von einem Netzlaufwerk gestartet werden können, es gibt immer mehr Anwendungen, die sich abseits der üblichen Installationswege auf einem Rechner ansammeln und auch regulär genutzt werden. Gerade in großen Netzwerken mit vielen Nutzern möchte man häufig auch solche Anwendungen anbieten, ohne gleich jedem Einzelnen die ganzen "offiziellen" Pakete lokal zu installieren. Legen Sie das Verzeichnis auf eine Netzwerkfreigabe und platzieren Sie einfach ein Icon auf dem Desktop oder im Startmenü des Nutzers und es wird funktionieren.


# Funktionsweise

Das hier angebotene Skript stellt sicher, dass die "portabel" angebotenen Anwendungen immer auf dem neuesten Stand sind und einfach verwaltet werden können. Neue Versionen werden automatisch gefunden, heruntergeladen und auf Wunsch auch installiert.

Das automatische Herunterladen, Überprüfen und Installieren oder Aktualisieren geht natürlich nur mit solchen Anwendungen, für die es einen Eintrag in der Datenbank gibt. In diesem wird beschrieben, wie Aktualisierungen erkannt werden, wie die Archive zu extrahieren und wo die einzelnen Dateien zu platzieren sind. Durch Nutzung dieser Datenbank ist es möglich, die Anwendungen ohne manuelle Zusatzaufwände (also eigene Installationen per Hand) zu nutzen. Es erscheint eine neue Version einer Anwendung? Das Skript erkennt das und aktualisiert diese von allein. Einzig starten müssen Sie es noch...

Automatisch erstellte Prüfsummen sorgen dafür, dass Veränderungen an den Verzeichnissen durch Dritte (oder den lokalen Virenschutz) leicht erkannt werden können. Das Skript extrahiert die normalen Anwendungen in ein Verzeichnis, welches (ggf. auch schreibgeschützt) den Nutzern angeboten werden kann. Einschränkungen oder Abweichungen bzgl. einer regulären Installation werden dokumentiert. Also wenn eine Anwendung etwa zwingend einen Laufwerksbuchstaben benötigt und nicht von einem UNC-Pfad aus gestartet werden kann oder wenn die Datenverzeichnisse lokal liegen (%APPDATA%).

Sie benötigen als Grundlage eine bash-Shell mit den folgenden Tools im Suchpfad: sed, diff, patch, sha256sum, wget (oder curl), git. Da das Ziel die Bereitstellung von Windows-Anwendungen ist, wird poappin vorzugsweise auch unter Windows mit Hilfe einer [MSYS2](https://www.msys2.org/)-Installation genutzt. Installieren Sie MSYS2 und die einzelnen Tools (die meisten dürften bereits dabei sein), dann nutzen Sie bspw. git um das Repository zu beschaffen.

Wenn Sie Fehler melden oder neue Funktionen oder Anwendungen anfordern möchten, verwenden Sie bitte unsere [Fehlermeldeplattform](https://gitlab.com/Munahid/poappin/-/issues).


# Installation

Benutze eine MSYS2 bash-Shell:

    git clone "https://gitlab.com/Munahid/poappin.git"
    cd poappin
    # Nimm `git pull` oder `poappin update database` zum Aktualisieren.


# Nutzung

    . poappin.sh
    
    poappin info all
    poappin install putty
    # Ein Verzeichnis "putty-0.74" erscheint unter "apps", setze Deine Verknüpfungen und Icons.
	poappin install firefox
	# Ein Verzeichnis "firefox-81.0.1 erscheint unter "apps", setze Deine Verknüpfungen und Icons.

    poappin remove putty


# Konfiguration

Um das Skript an Ihre Bedürfnisse anzupassen, fügen Sie einfach alle Änderungen zu einer Datei im Basisverzeichnis `config.h` hinzu.

Wenn das "apps"-Verzeichnis mit den installierten Anwendungen für andere Nutzer veröffentlicht werden soll, bietet es sich an, ein zweites Verzeichnis zu pflegen, in welchem die z.B. über eine Freigabe veröffentlichten Anwendungen ohne Versionsnummer enthalten sind:

    POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY="/c/Portable"
    POAPPIN_CONFIG_AUTO_ACTIVATE="true"

    poappin_add_icon_to_active_apps_dir

Fügen Sie nun zu diesem Verzeichnis Anwendungen (ohne eine Versionsnummer im Verzeichnamen) hinzu:

    poappin_activate putty 0.74

Dies erzeugt eine Verbindung von "putty" nach "apps/putty-0.74".

Auf diese Art und Weise können Sie Anwendungen mit verschiedene Versionen mittels "poappin update all" automatisiert installieren lassen und parallel vorhalten, aber den Nutzern nur eine erprobte Anwendung gezielt freischalten.


# Automatisierung

Sie können eine Verknüpfung (oder eine Aufgabe) erstellen, um ein Bash-Shell-Skript in MSYS2 auszuführen. Klicken Sie mit der rechten Maustaste auf Ihren Desktop oder in Windows Explorer, wählen Sie "Neu", dann "Verknüpfung" und geben Sie den Aufruf zur Bash und die erforderlichen Befehle für das Verknüpfungsziel ("Speicherort des Elements") ein:

    C:\msys64\usr\bin\env MSYSTEM=MINGW64 C:\msys64\usr\bin\bash -l -c ". /proxy.sh; . /c/poappin/poappin.sh; poappin update all; sleep 1000"

Dies öffnet ein Fenster. Wenn Sie eine völlig versteckte Ausgabe wünschen, versuchen Sie:

    C:\msys64\usr\bin\mintty.exe -w hide /bin/env ...


# Lizenz

poappin ist unter der [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.html) oder höher lizenziert.

Copyright (C) 2020-2022 Gerrit Albrecht <mail (at) gerrit-albrecht.de>

Dieses Programm ist freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, entweder Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren Version weitergeben und/oder ändern. Dieses Programm wird in der Hoffnung verbreitet, dass es nützlich sein wird, jedoch OHNE JEGLICHE GARANTIE; ohne die implizite Garantie der MARKTGÄNGIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK. Weitere Informationen finden Sie in der GNU General Public License. Sie sollten zusammen mit diesem Programm eine Kopie der GNU General Public License erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.


# Verweise

- [GitLab Projekt-Seiten](https://gitlab.com/Munahid/poappin/)
- [Fehlermeldeplattform](https://gitlab.com/Munahid/poappin/-/issues)
- [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.html)
- [MSYS2](https://www.msys2.org/)
