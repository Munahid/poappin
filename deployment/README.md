# The deployment directory

This directory contains files which you need in case you want to
use your poappin/apps directory as a base for a "portable" directory
with links to the current apps. The batch files allow you to not
only write-protect the files, better to change the owner and
protect them in a manner no other application or user is able to
modify files in it.

