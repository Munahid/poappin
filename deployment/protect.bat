@echo off

rem This script protects the selected directory by changing the
rem owner of the directory. The new owner becomes the "Portable"
rem user, the administrator and system users are deprived of all
rem rights. The script must be started as a user administrator!
rem Even administrators then no longer have access to the files.

ECHO Running as user '%USERNAME%'

SET DIR="D:\Portable"

REM Besitzer �ndern. Rechte werden nicht angefasst. Dazu ggf. takeown.exe nehmen.

icacls %DIR% /setowner Portable /T /C

REM Vererbung ausschalten.

icacls %DIR% /inheritance:d

REM Benutzer "Portable" hinzuf�gen und Vollzugriff gew�hren.

icacls %DIR% /grant:r Portable:(CI)(OI)(F)

REM "Authentifizierte Benutzer" sollen nichts �ndern oder
REM schreiben k�nnen. Einfaches Setzen von nur RX funktionierte
REM nicht. Entferne daher alle Rechte und setze dann nur RX.

icacls %DIR% /remove "Authentifizierte Benutzer"
icacls %DIR% /grant:r "Authentifizierte Benutzer":(CI)(OI)(RX)

REM Alle Rechte und Verbote f�r "Benutzer" entfernen. Auch
REM "Administratoren" und "SYSTEM" den Vollzugriff l�schen.
REM Erst SYSTEM entfernen. Vorsicht, nach dem Entfernen der
REM Administratoren-Rechte kann dieser Skript nichts mehr tun.

icacls %DIR% /remove Benutzer
icacls %DIR% /remove SYSTEM
icacls %DIR% /remove Administratoren

