# [Portable Applications Installer (poappin)](https://gitlab.com/Munahid/poappin/)

_The bash script "poappin" is a small command line tool that allows you to install applications from the internet and update them automatically without calling their setup or `msiexec /i`._


# Sense and purpose

Do you know that? You found a small program on the Internet or in a magazine, you find it practical and copy it somewhere into the file system. Then it will be forgotten until it is needed again at some point in time. Now it is actually needed and it no longer works: Because directory names or drive letters have changed in the meantime, the computer has been updated, the configuration has been changed, a colleague has deliberately removed it because it was not important enough for him or just because a virus protection program did not like one or the other file. You cannot rely on software that is not maintained and used.

The script ensures that your software is always procured anew and installed in the same way. The latest versions are always installed. No manufacturer-specific update software has to be started constantly in the background. Installations with administrator rights are unnecessary. The script ensures that you have a directory in which the software you need is always up-to-date. You just have to start the application.

Starting with small tools that come as a single, executable binary file, to large applications that can be easily started from a network drive without installation, there are more and more applications that accumulate on a computer apart from the usual installation methods and are also used regularly. Especially in large networks with many users, one would often like to offer such applications without installing all of the "official" packages locally. Put the directory on a network share and just place an icon on the desktop or in the user's start menu and it will work.


# Functionality

The script offered here ensures that the "portable" applications are always up to date and can be easily managed. New versions are automatically found, downloaded and installed on request.

Automatic downloading, checking and installing or updating is of course only possible with applications for which there is an entry in the database. This describes how updates are recognized, how the archives are extracted and where the individual files are to be placed. By using this database, it is possible to use the applications without additional manual effort (i.e. own installations by hand). A new version of an application appears? The script recognizes this and updates it automatically. All you have to do is start it ...

Automatically generated checksums ensure that changes to the directories by third parties (or a local virus protection software) can be easily recognized. The script extracts the normal applications into a directory, which can also be offered to users (possibly also write-protected). Restrictions or deviations with regard to a regular installation are documented. So if an application needs a drive letter and cannot be started from a UNC path or if the data directories are local (% APPDATA%).

As a basis you need a bash shell with the following tools in the search path: sed, diff, patch, sha256sum, wget (or curl), git. Since the goal is to provide Windows applications, poappin is preferably used under Windows with the help of an [MSYS2](https://www.msys2.org/) installation. Install MSYS2 and the individual tools (most should already be there), then use e.g. git to get the repository.

If you would like to report any bugs or request new functions or applications, please use our [issue tracker](https://gitlab.com/Munahid/poappin/-/issues).


# Installation

Use a MSYS2 bash shell:

    git clone "https://gitlab.com/Munahid/poappin.git"
    cd poappin
    # Use `git pull` or `poappin update database` to update.


# Usage

    . poappin.sh
    
    poappin info all
    poappin install putty
    # A directory "putty-0.74" appears in the "apps" directory.
	poappin install firefox
	# A directory "firefox-81.0.1 appears in the "apps" directory.

    poappin remove putty


# Configuration

To adapt the script to your needs, simply add all changes to a file `config.h` in the base directory.

If the "apps" directory with the installed applications is to be published for other users, it is advisable to maintain a second directory which contains the applications published e.g. via a share without a version number:

    POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY="/c/Portable"
    POAPPIN_CONFIG_AUTO_ACTIVATE="true"

    poappin_add_icon_to_active_apps_dir

Now add applications (without a version number in the directory name) to this direcory:

    poappin_activate putty 0.74

This creates a junction "putty" which refers to "apps/putty-0.74".

In this way, you can have applications with different versions installed automatically using "poappin update all" and have them available in parallel, but you can only activate one tried and tested application for the users.


# Automatization

You can create a shortcut (or a task) to run a Bash shell script in MSYS2. Right-click on your Desktop or in Windows Explorer, select "New", then select "Shortcut", and enter the call to bash and the needed commands for the shortcut target:

    C:\msys64\usr\bin\env MSYSTEM=MINGW64 C:\msys64\usr\bin\bash -l -c ". /proxy.sh; . /c/poappin/poappin.sh; poappin update all; sleep 1000"

This will open a window. If you want a completely hidden output, try:

    C:\msys64\usr\bin\mintty.exe -w hide /bin/env ...


# License

poappin is licensed under the [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.html) or later.

Copyright (C) 2020-2022 Gerrit Albrecht <mail (at) gerrit-albrecht.de>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


# Links

- [GitLab Project Pages](https://gitlab.com/Munahid/poappin/)
- [Issue Tracker](https://gitlab.com/Munahid/poappin/-/issues)
- [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.html)
- [MSYS2](https://www.msys2.org/)
