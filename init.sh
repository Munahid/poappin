#!/bin/bash

# Source this script to get all installed poappin-managed applications into
# your environment. After installing a portable package you will see a lot
# of them add themselves here (TODO!) to be found within the $PATH or to add
# needed environment variables.

DRIVELETTER=`pwd -W | cut -d':' -f1 | tr '[:upper:]' '[:lower:]'`
APPS_PATH=/${DRIVELETTER}/projects/poappin/apps

# BEGIN: 7-zip

export PATH=${PATH}:${APPS_PATH}/7-zip-22.01

# END: 7-zip


# BEGIN: eclipse-adoptium-jre

export JAVA_HOME=${APPS_PATH}/eclipse-adoptium-jre-11.0.16.1_1
export PATH=${PATH}:${JAVA_HOME}/bin

# END: eclipse-adoptium-jre


# BEGIN: fop

export PATH=${PATH}:${APPS_PATH}/fop-2.7/fop

# END: fop


# BEGIN: innoextract

export PATH=${PATH}:${APPS_PATH}/innoextract-1.9

# END: innoextract


# BEGIN: lessmsi

export PATH=${PATH}:${APPS_PATH}/lessmsi-1.10.0

# END: lessmsi


# BEGIN: wix

export PATH=${PATH}:${APPS_PATH}/wix-3.11.2

# END: wix


# BEGIN: windows-sdk

#export PATH=${PATH}:${APPS_PATH}/windows-sdk-10.0.19041.0
export PATH=${PATH}:'/c/Program Files (x86)/Windows Kits/10/bin/10.0.19041.0/x64'

# Beware: windows-sdk target only extracts the downloaded ISO file into
# the applications directory. You need to setup normally.
# Beware: There is also a signtool from MSYS2 for signing jar files.
# We need the version from Windows SDK - copied it into the tools directory.

# END: windows-sdk


# BEGIN: emscripten

source ${APPS_PATH}/emscripten-git/emsdk/emsdk_env.sh

# Beware: emscripten installs a Oracle JRE which may conflict with ours.

# END: emscripten


# BEGIN: php

export PATH=${PATH}:${APPS_PATH}/php-8.1.12

# END: php


# BEGIN: postgresql

export PATH=${PATH}:${APPS_PATH}/postgresql-13.1.1/bin
export PATH=${PATH}:${APPS_PATH}/postgresql-13.1.1/pgAdmin\ 4/bin/

# END: postgresql
